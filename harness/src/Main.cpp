// Standard headers
#include <iostream>
#include <fstream>

// 3rd party headers
#define GLFW_INCLUDE_VULKAN

#include <GLFW/glfw3.h>
#include <forge/Forge.h>

// Application headers
#include "Main.h"
#include "Image.h"

using namespace std;

//
// Entry point
//
int main(int argc, char** argv)
{
	//
	// Initialise GLFW
	//
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

	GLFWwindow* window = glfwCreateWindow(
		WINDOW_WIDTH, 
		WINDOW_HEIGHT, 
		"Forge harness", 
		nullptr, 
		nullptr);

	//
	// Initialise forge
	//

	// GLFW surface factory
	auto glfwSurfaceFactory = [window](VkInstance const& instance, VkSurfaceKHR* surface)
	{
		return glfwCreateWindowSurface(instance, window, nullptr, surface);
	};

	auto binaryFileLoader = [](string const& filepath)
	{
		ifstream fp(filepath, std::ios::ate | std::ios::binary);

		if (!fp.is_open()) {
			string errMsg = "Could not open file " + filepath;
			throw std::runtime_error(errMsg.c_str());
		}

		size_t fileSize = (size_t)fp.tellg();
		vector<char> buffer(fileSize);

		fp.seekg(0);
		fp.read(buffer.data(), fileSize);

		fp.close();

		return buffer;
	};

	auto imageFileLoader = [binaryFileLoader](string const& filepath)
	{
		Image image(filepath, binaryFileLoader(filepath));
		return vector<char>(image.getData(), image.getData() + image.getSize());
	};

	forge::Forge* forge = new forge::Forge("Harness", glfwSurfaceFactory, binaryFileLoader, imageFileLoader);
	glfwSetWindowUserPointer(window, forge);

	forge->addMessageHandler([](string const& msg, forge::MessageLevel level) {
		string text = "[?????]";

		if (level == forge::MessageLevel::Debug)
			text = "[DEBUG]";
		else if (level == forge::MessageLevel::Info)
			text = "[INFO ]";
		else if (level == forge::MessageLevel::Warning)
			text = "[WARN ]";
		else if (level == forge::MessageLevel::Error)
			text = "[ERROR]";
		else if (level == forge::MessageLevel::Fatal)
			text = "[FATAL]";

		cout << text.c_str() << " " << msg.c_str() << '\n';
	}, forge::MessageLevel::Fatal);

	// Handle resizes
	glfwSetFramebufferSizeCallback(window, [](GLFWwindow* window, int windowWidth, int windowHeight)
	{
		auto forge = static_cast<forge::Forge*>(glfwGetWindowUserPointer(window));
		forge->resizeWindow(windowWidth, windowHeight);
	});

	// Set validation
	bool enableValidationLayers = false;

#if _DEBUG
	enableValidationLayers = true;
#endif

	uint32_t numGlfwExtensions = 0;
	const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&numGlfwExtensions);

	vector<char const*> extensions(glfwExtensions, glfwExtensions + numGlfwExtensions);

	int exitCode = 0;

	try
	{
		forge->initialise(WINDOW_WIDTH, WINDOW_HEIGHT, extensions, enableValidationLayers);
		//forge->createVertexBuffer();
	}
	catch (exception const& e)
	{
		cout << e.what() << '\n';
		exitCode = 1;
		goto shutdown;
	}

	//
	// Main loop
	//
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
		forge->renderFrame();
	}

	//
	// Shutdown
	//
	shutdown:
	delete forge;

	glfwDestroyWindow(window);
	glfwTerminate();

	return exitCode;
}
