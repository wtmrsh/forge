#include <freeimage/FreeImage.h>

#include "Image.h"

using namespace std;

void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message)
{
	// Hook into logging
	// ...
}

Image::Image(string const& filename, vector<char> const& rawData)
	: mData(nullptr)
	, mSize(0)
	, mWidth(0)
	, mHeight(0)
	, mNumChannels(0)
{
	FIMEMORY* fibuf = FreeImage_OpenMemory((BYTE*)rawData.data(), rawData.size());
	FIBITMAP* bitmap = FreeImage_LoadFromMemory(FreeImage_GetFIFFromFilename(filename.c_str()), fibuf);

	if (bitmap)
	{
		// Check it's supported
		FREE_IMAGE_TYPE imageType = FreeImage_GetImageType(bitmap);

		if (imageType != FIT_BITMAP/* &&
			imageType != FIT_RGB16 &&
			imageType != FIT_RGBA16 &&
			imageType != FIT_RGBF &&
			imageType != FIT_RGBAF*/)
		{
			FreeImage_Unload(bitmap);
			throw exception("Unsupported image type.");
		}

		// Convert to 8 bit.
		if (imageType == FIT_RGB16 ||
			imageType == FIT_RGBA16 ||
			imageType == FIT_RGBF ||
			imageType == FIT_RGBAF)
		{
			bitmap = FreeImage_ColorQuantizeEx(bitmap, FIQ_WUQUANT);
			bitmap = FreeImage_ConvertTo8Bits(bitmap);
		}

		mWidth = (int)FreeImage_GetWidth(bitmap);
		mHeight = (int)FreeImage_GetHeight(bitmap);
		mNumChannels = (int)FreeImage_GetBPP(bitmap) / 8;

		uint32_t dataSpan = mWidth * mNumChannels;
		mSize = dataSpan * mHeight;

		mData = new uint8_t[mSize];
		memcpy(mData, (uint8_t*)FreeImage_GetBits(bitmap), mSize * sizeof(uint8_t));

		FreeImage_Unload(bitmap);
	}
	else
	{
		// Error
		throw exception("Could not load image.");
	}
}

Image::~Image()
{
	delete[] mData;
}

uint8_t const* Image::getData() const
{
	return mData;
}

uint32_t Image::getSize() const
{
	return mSize;
}

int Image::getWidth() const
{
	return mWidth;
}

int Image::getHeight() const
{
	return mHeight;
}

int Image::getNumChannels() const
{
	return mNumChannels;
}
