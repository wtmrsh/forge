#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform sampler2D tex;

layout(location = 0) in vec2 vs_outUV;
layout(location = 1) in vec3 vs_outColour;

layout(location = 0) out vec4 fs_outColour;

void main()
{
	fs_outColour = texture(tex, vs_outUV);
}
