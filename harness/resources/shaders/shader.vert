#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} mvp;

layout(location = 0) in vec3 vs_inPosition;
layout(location = 1) in vec2 vs_inUV;
layout(location = 2) in vec3 vs_inColour;

layout(location = 0) out vec2 vs_outUV;
layout(location = 1) out vec3 vs_outColour;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
	gl_Position = gl_Position = mvp.proj * mvp.view * mvp.model * vec4(vs_inPosition, 1.0);
	vs_outUV = vs_inUV;
	vs_outColour = vs_inColour;
}
