#pragma once

#include <string>
#include <cstdint>
#include <vector>

class Image
{
	uint8_t* mData;

	uint32_t mSize;

	int mWidth;

	int mHeight;

	int mNumChannels;

public:

	Image(std::string const& filename, std::vector<char> const& rawData);

	~Image();

	uint8_t const* getData() const;

	uint32_t getSize() const;

	int getWidth() const;

	int getHeight() const;

	int getNumChannels() const;
};

