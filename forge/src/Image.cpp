#include "forge/Image.h"
#include "forge/Buffer.h"
#include "forge/ForgeHelper.h"
#include "forge/Debug.h"
#include "forge/VulkanException.h"

namespace forge
{

	using namespace std;

	//
	// Image()
	//
	Image::Image(LogicalDevice* logicalDevice, vector<char> const& data, int width, int height, int numChannels)
		: mLogicalDevice(logicalDevice)
		, mData(data)
		, mWidth(width)
		, mHeight(height)
		, mNumChannels(numChannels)
		, mImage(VK_NULL_HANDLE)
		, mImageMemory(VK_NULL_HANDLE)
		, mImageView(VK_NULL_HANDLE)
	{
	}

	//
	// ~Image()
	//
	Image::~Image()
	{
		destroy();
	}

	//
	// getWidth()
	//
	int Image::getWidth() const
	{
		return mWidth;
	}

	//
	// getHeight()
	//
	int Image::getHeight() const
	{
		return mHeight;
	}

	//
	// getNumChannels()
	//
	int Image::getNumChannels() const
	{
		return mNumChannels;
	}

	//
	// getImageView()
	//
	VkImageView Image::getImageView() const
	{
		return mImageView;
	}

	//
	// create()
	//
	void Image::create(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue, bool clearData)
	{
		VkDeviceSize imageSize = mWidth * mHeight * mNumChannels;

		// Create staging buffer
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;

		ForgeHelper::createBuffer(
			physicalDevice,
			mLogicalDevice,
			imageSize,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			stagingBuffer,
			stagingBufferMemory);

		void* data;
		vkMapMemory(mLogicalDevice->getDevice(), stagingBufferMemory, 0, imageSize, 0, &data);
		memcpy(data, mData.data(), mData.size());
		vkUnmapMemory(mLogicalDevice->getDevice(), stagingBufferMemory);

		// Clear data
		if (clearData)
		{
			mData.clear();
		}

		// Create image.  Only 3 and 4 channels supported at the moment.
		VkFormat dstFormat = mNumChannels == 3 ? VK_FORMAT_R8G8B8_UNORM : VK_FORMAT_R8G8B8A8_UNORM;

		createImage(
			physicalDevice,
			dstFormat,
			VK_IMAGE_TILING_OPTIMAL,
			VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		// Transition image layout
		transitionImageLayout(pool, queue, dstFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		copyBufferToImage(pool, queue, stagingBuffer);
		transitionImageLayout(pool, queue, dstFormat, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		// Destroy staging buffer
		vkDestroyBuffer(mLogicalDevice->getDevice(), stagingBuffer, nullptr);
		vkFreeMemory(mLogicalDevice->getDevice(), stagingBufferMemory, nullptr);

		// Create image view
		createImageView(mNumChannels == 3 ? VK_FORMAT_R8G8B8_UNORM : VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
	}

	//
	// destroy()
	//
	void Image::destroy()
	{
		vkDestroyImageView(mLogicalDevice->getDevice(), mImageView, nullptr);
		mImageView = VK_NULL_HANDLE;

		vkDestroyImage(mLogicalDevice->getDevice(), mImage, nullptr);
		mImage = VK_NULL_HANDLE;

		vkFreeMemory(mLogicalDevice->getDevice(), mImageMemory, nullptr);
		mImageMemory = VK_NULL_HANDLE;
	}

	//
	// createImage()
	//
	void Image::createImage(PhysicalDevice* physicalDevice, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties)
	{
		VkImageCreateInfo imageInfo = {};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.extent.width = mWidth;
		imageInfo.extent.height = mHeight;
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.format = format;
		imageInfo.tiling = tiling;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageInfo.usage = usage;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		checkResult(vkCreateImage(mLogicalDevice->getDevice(), &imageInfo, nullptr, &mImage), "Could not create image.");

		VkMemoryRequirements memRequirements;
		vkGetImageMemoryRequirements(mLogicalDevice->getDevice(), mImage, &memRequirements);

		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = physicalDevice->getMemoryType(memRequirements.memoryTypeBits, properties);

		checkResult(vkAllocateMemory(mLogicalDevice->getDevice(), &allocInfo, nullptr, &mImageMemory), "Could not allocate image memory.");

		vkBindImageMemory(mLogicalDevice->getDevice(), mImage, mImageMemory, 0);
	}

	//
	// transitionImageLayout()
	//
	void Image::transitionImageLayout(VkCommandPool pool, VkQueue queue, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout)
	{
		VkCommandBuffer commandBuffer = ForgeHelper::beginSingleTimeCommands(mLogicalDevice, pool);

		VkImageMemoryBarrier barrier = {};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = oldLayout;
		barrier.newLayout = newLayout;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.image = mImage;

		if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

			if (hasStencilComponent(format)) {
				barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
			}
		}
		else {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		}

		barrier.subresourceRange.baseMipLevel = 0;
		barrier.subresourceRange.levelCount = 1;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = 1;

		VkPipelineStageFlags sourceStage;
		VkPipelineStageFlags destinationStage;

		if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
		{
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		}
		else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) 
		{
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		}
		else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
		{
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		}
		else
		{
			throw VulkanException("Unsupported layout transition.");
		}

		vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
		ForgeHelper::endSingleTimeCommands(commandBuffer, mLogicalDevice, pool, queue);
	}

	//
	// copyBufferToImage()
	//
	void Image::copyBufferToImage(VkCommandPool pool, VkQueue queue, VkBuffer buffer)
	{
		VkCommandBuffer commandBuffer = ForgeHelper::beginSingleTimeCommands(mLogicalDevice, pool);

		VkBufferImageCopy region = {};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;
		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = 1;
		region.imageOffset = { 0, 0, 0 };
		region.imageExtent = { (uint32_t)mWidth, (uint32_t)mHeight, 1 };

		vkCmdCopyBufferToImage(commandBuffer, buffer, mImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

		ForgeHelper::endSingleTimeCommands(commandBuffer, mLogicalDevice, pool, queue);
	}

	//
	// createImageView()
	//
	void Image::createImageView(VkFormat format, VkImageAspectFlags aspectFlags)
	{
		mImageView = ForgeHelper::createImageView(mLogicalDevice, mImage, format, aspectFlags);
	}

	//
	// hasStencilComponent()
	//
	bool Image::hasStencilComponent(VkFormat format) const
	{
		return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
	}

} // forge

