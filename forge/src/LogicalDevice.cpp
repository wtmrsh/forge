#include <set>

#include "forge/LogicalDevice.h"
#include "forge/Debug.h"

namespace forge
{
	using namespace std;

	//
	// LogicalDevice()
	//
	LogicalDevice::LogicalDevice(PhysicalDevice const& physicalDevice, VkSurfaceKHR surface, LogicalDeviceParameters const& parameters)
		: mParameters(parameters)
		, mDevice(VK_NULL_HANDLE)
	{
		create(physicalDevice, surface);
	}

	//
	// ~LogicalDevice()
	//
	LogicalDevice::~LogicalDevice()
	{
		vkDestroyDevice(mDevice, nullptr);
		mDevice = VK_NULL_HANDLE;
	}

	//
	// create()
	//
	void LogicalDevice::create(PhysicalDevice const& physicalDevice, VkSurfaceKHR surface)
	{
		message(MessageLevel::Info, "Creating logical device.");

		QueueFamilyIndices indices = physicalDevice.getQueueFamilies(surface);

		vector<VkDeviceQueueCreateInfo> queueCreateInfos;
		set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };

		float queuePriority = 1.0f;
		for (uint32_t queueFamily: uniqueQueueFamilies)
		{
			VkDeviceQueueCreateInfo queueCreateInfo = {};

			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfo.queueFamilyIndex = queueFamily;
			queueCreateInfo.queueCount = 1;
			queueCreateInfo.pQueuePriorities = &queuePriority;

			queueCreateInfos.push_back(queueCreateInfo);
		}

		// Struct to hold required device features.
		VkPhysicalDeviceFeatures deviceFeatures = {};
		deviceFeatures.samplerAnisotropy = VK_TRUE;

		// Create the logical device
		VkDeviceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

		createInfo.pQueueCreateInfos = queueCreateInfos.data();
		createInfo.queueCreateInfoCount = (uint32_t)queueCreateInfos.size();

		createInfo.pEnabledFeatures = &deviceFeatures;

		createInfo.enabledExtensionCount = (uint32_t)mParameters.requiredExtensions.size();
		createInfo.ppEnabledExtensionNames = mParameters.requiredExtensions.data();

		// Set validation layers
		if (mParameters.requiredValidationLayers.size() > 0)
		{
			createInfo.enabledLayerCount = (uint32_t)mParameters.requiredValidationLayers.size();
			createInfo.ppEnabledLayerNames = mParameters.requiredValidationLayers.data();
		}
		else
		{
			createInfo.enabledLayerCount = 0;
		}

		// Create device
		mDevice = physicalDevice.createLogicalDevice(createInfo);
	}

	//
	// getDevice()
	//
	VkDevice LogicalDevice::getDevice() const
	{
		return mDevice;
	}

} // forge