#include "forge/Buffer.h"
#include "forge/ForgeHelper.h"
#include "forge/Debug.h"

namespace forge
{

	//
	// Buffer()
	//
	Buffer::Buffer(LogicalDevice* logicalDevice)
		: mLogicalDevice(logicalDevice)
		, mBuffer(VK_NULL_HANDLE)
		, mBufferMemory(VK_NULL_HANDLE)
	{
	}

	//
	// getBuffer()
	//
	VkBuffer Buffer::getBuffer() const
	{
		return mBuffer;
	}

	//
	// getBufferMemory()
	//
	VkDeviceMemory Buffer::getBufferMemory() const
	{
		return mBufferMemory;
	}

} // forge
