#include <set>
#include <string>

#include <utils/StringUtils.h>

#include "forge/Instance.h"
#include "forge/VulkanException.h"
#include "forge/BadResultCode.h"
#include "forge/Debug.h"
#include "forge/Forge.h"

namespace forge
{
	using namespace std;

	//
	// Instance()
	//
	Instance::Instance(InstanceParameters const& parameters)
		: mParameters(parameters)
		, mInstance(VK_NULL_HANDLE)
		, mDebugMessenger(VK_NULL_HANDLE)
	{
		checkExtensionSupport(parameters.requiredExtensions);

		if (parameters.requiredValidationLayers.size() > 0)
		{
			checkValidationLayers(parameters.requiredValidationLayers);
		}

		create();
	}

	//
	// ~Instance()
	//
	Instance::~Instance()
	{
		vkDestroyInstance(mInstance, nullptr);
		mInstance = VK_NULL_HANDLE;
	}

	//
	// checkExtensionSupport()
	//
	void Instance::checkExtensionSupport(vector<char const*> const& requiredExtensions)
	{
		// Get vulkan extensions
		//message(MessageLevel::Info, "Retrieving supported instance extensions.");

		uint32_t numAvailableExtensions;
		vkEnumerateInstanceExtensionProperties(nullptr, &numAvailableExtensions, nullptr);

		vector<VkExtensionProperties> availableExtensions(numAvailableExtensions);
		vkEnumerateInstanceExtensionProperties(nullptr, &numAvailableExtensions, availableExtensions.data());

		// Check to see if all required extensions are supported
		set<string> extensionNames;
		for (auto const& ext: availableExtensions)
		{
			//message(MessageLevel::Info, string("  ") + string(ext.extensionName) + " v" + utils::StringUtils::toString(ext.specVersion));
			extensionNames.insert(ext.extensionName);
		}

		vector<string> missingExtensions;
		for (auto const& ext: requiredExtensions)
		{
			string reqExt(ext);
			if (extensionNames.find(reqExt) == extensionNames.end())
			{
				missingExtensions.push_back(reqExt);
			}
		}

		if (!missingExtensions.empty())
		{
			string missing = utils::StringUtils::join(missingExtensions.begin(), missingExtensions.end(), ",");
			throw VulkanException("The following required extension(s) are not supported: " + missing);
		}
	}

	//
	// checkValidationLayers()
	//
	void Instance::checkValidationLayers(vector<char const*> const& requiredValidationLayers)
	{
		// Get validation layers
		//message(MessageLevel::Info, "Setting validation layers.");

		uint32_t numAvailableLayers;
		vkEnumerateInstanceLayerProperties(&numAvailableLayers, nullptr);

		vector<VkLayerProperties> availableValidationLayers(numAvailableLayers);
		vkEnumerateInstanceLayerProperties(&numAvailableLayers, availableValidationLayers.data());

		// Check to see if all required extensions are supported
		set<string> layerNames;
		for (auto const& layer: availableValidationLayers)
		{
			//message(MessageLevel::Info, string("  ") + string(layer.layerName) + " v" + utils::StringUtils::toString(layer.specVersion));
			//message(MessageLevel::Info, string("  - ") + string(layer.description));
			layerNames.insert(layer.layerName);
		}

		vector<string> missingLayers;
		for (auto const& layer: requiredValidationLayers)
		{
			string reqLayer(layer);
			if (layerNames.find(reqLayer) == layerNames.end())
			{
				missingLayers.push_back(reqLayer);
			}
		}

		if (!missingLayers.empty())
		{
			string missing = utils::StringUtils::join(missingLayers.begin(), missingLayers.end(), ",");
			throw VulkanException("The following required validation layers(s) are not supported: " + missing);
		}
	}

	//
	// create()
	//
	void Instance::create()
	{
		//message(MessageLevel::Info, "Creating Vulkan instance.");

		// Application info
		VkApplicationInfo appInfo = {};

		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = mParameters.applicationName.c_str();
		appInfo.applicationVersion = VK_MAKE_VERSION(mParameters.applicationVersionMajor, mParameters.applicationVersionMinor, mParameters.applicationVersionPatch);
		
		appInfo.pEngineName = mParameters.engineName.c_str();
		appInfo.engineVersion = VK_MAKE_VERSION(mParameters.engineVersionMajor, mParameters.engineVersionMinor, mParameters.engineVersionPatch);
		
		appInfo.apiVersion = VK_API_VERSION_1_0;

		// Instance creation info
		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;

		createInfo.enabledExtensionCount = mParameters.requiredExtensions.size();
		createInfo.ppEnabledExtensionNames = mParameters.requiredExtensions.data();

		createInfo.enabledLayerCount = 0;

		// Do we want validation layers?
		if (mParameters.requiredValidationLayers.size() > 0)
		{
			//message(MessageLevel::Info, "- Enabling validation layers.");
			createInfo.enabledLayerCount = (uint32_t)mParameters.requiredValidationLayers.size();
			createInfo.ppEnabledLayerNames = mParameters.requiredValidationLayers.data();
		}
		else
		{
			createInfo.enabledLayerCount = 0;
		}

		// Create instance
		checkResult(vkCreateInstance(&createInfo, nullptr, &mInstance), "Cannot create vulkan instance");
	}

	//
	// getPhysicalDevices()
	//
	vector<VkPhysicalDevice> Instance::getPhysicalDevices() const
	{
		uint32_t numDevices = 0;
		vkEnumeratePhysicalDevices(mInstance, &numDevices, nullptr);

		if (numDevices == 0)
		{
			throw VulkanException("No devices with Vulkan support found.");
		}

		vector<VkPhysicalDevice> devices(numDevices);
		vkEnumeratePhysicalDevices(mInstance, &numDevices, devices.data());

		return devices;
	}

	//
	// createSurface()
	//
	VkSurfaceKHR Instance::createSurface(SurfaceFactory surfaceFactory)
	{
		VkSurfaceKHR surface;
		checkResult(surfaceFactory(mInstance, &surface), "Cannot create surface.");

		return surface;
	}

	//
	// destroySurface()
	//
	void Instance::destroySurface(VkSurfaceKHR* surface)
	{
		vkDestroySurfaceKHR(mInstance, *surface, nullptr);
		*surface = VK_NULL_HANDLE;
	}

	//
	// createDebugMessenger()
	//
	void Instance::createDebugMessenger()
	{
		//message(MessageLevel::Info, "Creating debug messenger.");
		
		VkDebugUtilsMessengerCreateInfoEXT debugMsgCreateInfo = {};
		debugMsgCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;

		debugMsgCreateInfo.messageSeverity = mParameters.debugMessageSeverities;
		debugMsgCreateInfo.messageType = mParameters.debugMessageTypes;
		debugMsgCreateInfo.pfnUserCallback = debugCallback;
		debugMsgCreateInfo.pUserData = this;

		// Get the function address
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(mInstance, "vkCreateDebugUtilsMessengerEXT");
		if (func)
		{
			checkResult(func(mInstance, &debugMsgCreateInfo, nullptr, &mDebugMessenger), "Cannot create debug messenger.");
		}
		else
		{
			throw BadResultCode(VK_ERROR_EXTENSION_NOT_PRESENT, "Cannot create debug messenger.");
		}
	}

	//
	// destroyDebugMessenger()
	//
	void Instance::destroyDebugMessenger()
	{
		//message(MessageLevel::Info, "Destroying debug messenger.");

		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(mInstance, "vkDestroyDebugUtilsMessengerEXT");
		if (func)
		{
			func(mInstance, mDebugMessenger, nullptr);
		}
		else
		{
			throw BadResultCode(VK_ERROR_EXTENSION_NOT_PRESENT, "Cannot destroy debug messenger.");
		}
	}

	//
	// debugCallback()
	//
	VKAPI_ATTR VkBool32 VKAPI_CALL Instance::debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData,
		void* pUserData)
	{
		MessageLevel level = MessageLevel::Info;

		switch (messageSeverity)
		{
		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
			level = MessageLevel::Debug; break;

		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
			level = MessageLevel::Info; break;

		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
			level = MessageLevel::Warning; break;

		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
			level = MessageLevel::Error; break;
		}

		Forge* f = static_cast<Forge*>(pUserData);
		//f->message(level, string("Validation layer: ") + pCallbackData->pMessage);

		return VK_FALSE;
	}

} // forge