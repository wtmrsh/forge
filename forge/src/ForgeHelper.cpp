#include "forge/ForgeHelper.h"
#include "forge/Debug.h"

namespace forge
{

	//
	// createBuffer()
	//
	void ForgeHelper::createBuffer(PhysicalDevice* physicalDevice, LogicalDevice* logicalDevice, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory)
	{
		VkBufferCreateInfo bufferInfo = {};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = size;
		bufferInfo.usage = usage;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		checkResult(vkCreateBuffer(logicalDevice->getDevice(), &bufferInfo, nullptr, &buffer), "Could not create buffer.");

		VkMemoryRequirements memRequirements;
		vkGetBufferMemoryRequirements(logicalDevice->getDevice(), buffer, &memRequirements);

		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = physicalDevice->getMemoryType(memRequirements.memoryTypeBits, properties);

		checkResult(vkAllocateMemory(logicalDevice->getDevice(), &allocInfo, nullptr, &bufferMemory), "Could not allocate buffer memory.");
		vkBindBufferMemory(logicalDevice->getDevice(), buffer, bufferMemory, 0);
	}

	//
	// copyBuffer()
	//
	void ForgeHelper::copyBuffer(LogicalDevice* logicalDevice, VkCommandPool pool, VkQueue queue, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
	{
		VkCommandBuffer commandBuffer = beginSingleTimeCommands(logicalDevice, pool);

		VkBufferCopy copyRegion = {};
		copyRegion.size = size;
		vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

		endSingleTimeCommands(commandBuffer, logicalDevice, pool, queue);
	}

	//
	// beginSingleTimeCommands()
	//
	VkCommandBuffer ForgeHelper::beginSingleTimeCommands(LogicalDevice* logicalDevice, VkCommandPool pool)
	{
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = pool;
		allocInfo.commandBufferCount = 1;

		VkCommandBuffer commandBuffer;
		vkAllocateCommandBuffers(logicalDevice->getDevice(), &allocInfo, &commandBuffer);

		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		vkBeginCommandBuffer(commandBuffer, &beginInfo);

		return commandBuffer;
	}

	//
	// endSingleTimeCommands()
	//
	void ForgeHelper::endSingleTimeCommands(VkCommandBuffer commandBuffer, LogicalDevice* logicalDevice, VkCommandPool pool, VkQueue queue)
	{
		vkEndCommandBuffer(commandBuffer);

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffer;

		vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
		vkQueueWaitIdle(queue);

		vkFreeCommandBuffers(logicalDevice->getDevice(), pool, 1, &commandBuffer);
	}

	//
	// createImageView()
	//
	VkImageView ForgeHelper::createImageView(LogicalDevice* logicalDevice, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags)
	{
		VkImageViewCreateInfo viewInfo = {};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = image;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = format;

		viewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

		viewInfo.subresourceRange.aspectMask = aspectFlags;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = 1;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = 1;

		VkImageView imageView;
		checkResult(vkCreateImageView(logicalDevice->getDevice(), &viewInfo, nullptr, &imageView), "Could not create image view.");

		return imageView;
	}
} // forge
