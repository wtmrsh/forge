#include "forge/VertexBuffer.h"
#include "forge/ForgeHelper.h"
#include "forge/VulkanException.h"
#include "forge/BadResultCode.h"
#include "forge/Debug.h"

namespace forge
{
	 
	//
	// VertexBuffer()
	//
	VertexBuffer::VertexBuffer(LogicalDevice* logicalDevice, bool indexed)
		: Buffer(logicalDevice)
		, mIndexed(indexed)
		, mIndexBuffer(VK_NULL_HANDLE)
		, mIndexBufferMemory(VK_NULL_HANDLE)
	{
	}

	//
	// ~VertexBuffer()
	//
	VertexBuffer::~VertexBuffer()
	{
		destroy();
	}

	//
	// isIndexed()
	//
	bool VertexBuffer::isIndexed() const
	{
		return mIndexed;
	}

	//
	// getIndexBuffer()
	//
	VkBuffer VertexBuffer::getIndexBuffer() const
	{
		return mIndexBuffer;
	}

	//
	// getNumVertices()
	//
	uint32_t VertexBuffer::getNumVertices() const
	{
		return (uint32_t)mVertices.size();
	}

	//
	// getNumIndices()
	//
	uint32_t VertexBuffer::getNumIndices() const
	{
		return (uint32_t)mIndices.size();
	}

	//
	// create()
	//
	void VertexBuffer::create(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue)
	{
		// This releases any handles/memory if they're already created, so we essentially recreate
		// the buffer.  To improve performance, we can cache its created state.
		destroy();

		// Create vertex buffer
		createDataBuffer(physicalDevice, pool, queue);

		// Create index buffer
		if (mIndexed)
		{
			createIndexBuffer(physicalDevice, pool, queue);
		}
	}

	//
	// destroy()
	//
	void VertexBuffer::destroy()
	{
		// Destroy index buffer
		if (mIndexed)
		{
			vkDestroyBuffer(mLogicalDevice->getDevice(), mIndexBuffer, nullptr);
			mIndexBuffer = VK_NULL_HANDLE;

			vkFreeMemory(mLogicalDevice->getDevice(), mIndexBufferMemory, nullptr);
			mIndexBufferMemory = VK_NULL_HANDLE;
		}

		// Destroy data buffer
		vkDestroyBuffer(mLogicalDevice->getDevice(), mBuffer, nullptr);
		mBuffer = VK_NULL_HANDLE;

		vkFreeMemory(mLogicalDevice->getDevice(), mBufferMemory, nullptr);
		mBufferMemory = VK_NULL_HANDLE;
	}

	//
	// createDataBuffer()
	//
	void VertexBuffer::createDataBuffer(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue)
	{
		// Vertex data
		mVertices =
		{
			{{-0.5f, -0.5f, 0.0f}, {0.0f, 0.0f}, {1.0f, 1.0f, 1.0f}},
			{{0.5f, -0.5f, 0.0f}, {1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}},
			{{0.5f, 0.5f, 0.0f}, {1.0f, 1.0f}, {1.0f, 1.0f, 1.0f}},
			{{-0.5f, 0.5f, 0.0f}, {0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},

			{ {-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}, {1.0f, 1.0f, 1.0f}},
			{{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}},
			{{0.5f, 0.5f, -0.5f}, {1.0f, 1.0f}, {1.0f, 1.0f, 1.0f}},
			{{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}}
		};

		VkDeviceSize bufferSize = sizeof(mVertices[0]) * mVertices.size();

		// Create staging buffer
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		ForgeHelper::createBuffer(
			physicalDevice,
			mLogicalDevice,
			bufferSize,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			stagingBuffer,
			stagingBufferMemory);

		// Copy vertex data into staging buffer
		void* data;
		vkMapMemory(mLogicalDevice->getDevice(), stagingBufferMemory, 0, bufferSize, 0, &data);
		memcpy(data, mVertices.data(), (size_t)bufferSize);
		vkUnmapMemory(mLogicalDevice->getDevice(), stagingBufferMemory);

		// Create data buffer and copy into it from staging buffer
		ForgeHelper::createBuffer(
			physicalDevice,
			mLogicalDevice,
			bufferSize,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			mBuffer,
			mBufferMemory);

		ForgeHelper::copyBuffer(mLogicalDevice, pool, queue, stagingBuffer, mBuffer, bufferSize);

		// Clean up staging buffer
		vkDestroyBuffer(mLogicalDevice->getDevice(), stagingBuffer, nullptr);
		vkFreeMemory(mLogicalDevice->getDevice(), stagingBufferMemory, nullptr);
	}

	//
	// createIndexBuffer()
	//
	void VertexBuffer::createIndexBuffer(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue)
	{
		// Index data
		mIndices = 
		{ 
			0, 1, 2, 2, 3, 0,
			4, 5, 6, 6, 7, 4
		};

		VkDeviceSize bufferSize = sizeof(mIndices[0]) * mIndices.size();

		// Create staging buffer
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		ForgeHelper::createBuffer(
			physicalDevice,
			mLogicalDevice,
			bufferSize,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			stagingBuffer,
			stagingBufferMemory);

		// Copy index data into staging buffer
		void* data;
		vkMapMemory(mLogicalDevice->getDevice(), stagingBufferMemory, 0, bufferSize, 0, &data);
		memcpy(data, mIndices.data(), (size_t)bufferSize);
		vkUnmapMemory(mLogicalDevice->getDevice(), stagingBufferMemory);

		// Create index buffer and copy into it from staging buffer
		ForgeHelper::createBuffer(
			physicalDevice,
			mLogicalDevice,
			bufferSize,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			mIndexBuffer,
			mIndexBufferMemory);

		ForgeHelper::copyBuffer(mLogicalDevice, pool, queue, stagingBuffer, mIndexBuffer, bufferSize);

		// Clean up staging buffer
		vkDestroyBuffer(mLogicalDevice->getDevice(), stagingBuffer, nullptr);
		vkFreeMemory(mLogicalDevice->getDevice(), stagingBufferMemory, nullptr);
	}

} // forge
