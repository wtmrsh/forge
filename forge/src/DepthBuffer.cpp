#include "forge/DepthBuffer.h"
#include "forge/ForgeHelper.h"
#include "forge/Debug.h"
#include "forge/VulkanException.h"

namespace forge
{

	using namespace std;

	//
	// DepthBuffer()
	//
	DepthBuffer::DepthBuffer(LogicalDevice* logicalDevice, int width, int height)
		: Image(logicalDevice, vector<char>(), width, height, 1)
	{
	}

	//
	// create()
	//
	void DepthBuffer::create(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue)
	{
		VkFormat depthFormat = getDepthFormat(physicalDevice);

		createImage(physicalDevice, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		createImageView(depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);

		transitionImageLayout(pool, queue, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
	}

	//
	// getDepthFormat()
	//
	VkFormat DepthBuffer::getDepthFormat(PhysicalDevice* physicalDevice) const
	{
		return physicalDevice->getSupportedFormat(
			{ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
			VK_IMAGE_TILING_OPTIMAL,
			VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
		);
	}

} // forge

