#include <map>
#include <set>

#include <utils/StringUtils.h>

#include "forge/PhysicalDevice.h"
#include "forge/VulkanException.h"
#include "forge/Debug.h"

namespace forge
{
	using namespace std;

	//
	// PhysicalDevice()
	//
	PhysicalDevice::PhysicalDevice(vector<VkPhysicalDevice> const& choices, VkSurfaceKHR surface, PhysicalDeviceParameters const& parameters)
		: mParameters(parameters)
		, mDevice(VK_NULL_HANDLE)
	{
		// Score all devices, and pick best.
		multimap<int, VkPhysicalDevice> deviceScores;
		for (auto const& device: choices)
		{
			VkPhysicalDeviceProperties properties;
			vkGetPhysicalDeviceProperties(device, &properties);

			message(MessageLevel::Info, " - " + string(properties.deviceName));

			if (isSuitable(device, surface, parameters.requiredExtensions))
			{
				int score = getScore(device, properties);
				deviceScores.insert(make_pair(score, device));
			}
		}

		if (deviceScores.rbegin()->first > 0)
		{
			mDevice = deviceScores.rbegin()->second;

			VkPhysicalDeviceProperties properties;
			vkGetPhysicalDeviceProperties(mDevice, &properties);

			message(MessageLevel::Info, " - Chosen: " + string(properties.deviceName));
		}
		else
		{
			throw VulkanException("No devices with adequate Vulkan support found.");
		}
	}

	//
	// ~LogicalDevice()
	//
	PhysicalDevice::~PhysicalDevice()
	{
		// It would appear that no explicit destruction of the physical device is required.
	}

	//
	// isSuitable()
	//
	bool PhysicalDevice::isSuitable(VkPhysicalDevice device, VkSurfaceKHR surface, vector<char const*> const& extensions) const
	{
		// We need the required queue familiies
		auto queueIndices = _getQueueFamilies(device, surface);
		if (!queueIndices.isComplete())
		{
			return false;
		}

		// We need the following extensions
		try
		{
			checkExtensionSupport(device, extensions);
		}
		catch (exception const&)
		{
			return false;
		}

		// Check swapchain
		SwapchainSupport swapchainSupport = _getSwapchainSupport(device, surface);
		if (swapchainSupport.formats.empty() || swapchainSupport.presentModes.empty())
		{
			return false;
		}

		// Device features
		VkPhysicalDeviceFeatures supportedFeatures;
		vkGetPhysicalDeviceFeatures(device, &supportedFeatures);

		if (!supportedFeatures.samplerAnisotropy)
		{
			return false;
		}

		return true;
	}

	//
	// checkExtensionSupport()
	//
	void PhysicalDevice::checkExtensionSupport(VkPhysicalDevice device, vector<char const*> const& requiredExtensions) const
	{
		// Get device extensions
		message(MessageLevel::Info, "Retrieving supported device extensions.");

		uint32_t numExtensions;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &numExtensions, nullptr);

		vector<VkExtensionProperties> extensions(numExtensions);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &numExtensions, extensions.data());

		// Check to see if all required extensions are supported
		set<string> extensionNames;
		for (auto const& ext : extensions)
		{
			message(MessageLevel::Info, string("  ") + string(ext.extensionName) + " v" + utils::StringUtils::toString(ext.specVersion));
			extensionNames.insert(ext.extensionName);
		}

		vector<string> missingExtensions;
		for (auto const& ext: requiredExtensions)
		{
			string reqExt(ext);
			if (extensionNames.find(reqExt) == extensionNames.end())
			{
				missingExtensions.push_back(reqExt);
			}
		}

		if (!missingExtensions.empty())
		{
			string missing = utils::StringUtils::join(missingExtensions.begin(), missingExtensions.end(), ",");
			throw VulkanException("The following required extension(s) are not supported: " + missing);
		}
	}

	//
	// getScore()
	//
	int PhysicalDevice::getScore(VkPhysicalDevice device, VkPhysicalDeviceProperties properties) const
	{
		int score = 1;

		if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
		{
			score += 1000;
		}

		return score;
	}

	//
	// _getQueueFamilies()
	//
	QueueFamilyIndices PhysicalDevice::_getQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface) const
	{
		// Get queue families
		QueueFamilyIndices indices;

		uint32_t numQueueFamilies = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &numQueueFamilies, nullptr);

		vector<VkQueueFamilyProperties> queueFamilies(numQueueFamilies);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &numQueueFamilies, queueFamilies.data());

		int i = 0;
		for (auto const& queueFamily: queueFamilies)
		{
			// Check for graphics queue
			if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				indices.graphicsFamily = i;
			}

			// Check for present queue
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

			if (queueFamily.queueCount > 0 && presentSupport)
			{
				indices.presentFamily = i;
			}

			if (indices.isComplete())
			{
				break;
			}

			i++;
		}

		return indices;
	}

	//
	// getQueueFamilies()
	//
	QueueFamilyIndices PhysicalDevice::getQueueFamilies(VkSurfaceKHR surface) const
	{
		return _getQueueFamilies(mDevice, surface);
	}

	//
	// _getSwapchainSupport()
	//
	SwapchainSupport PhysicalDevice::_getSwapchainSupport(VkPhysicalDevice device, VkSurfaceKHR surface) const
	{
		SwapchainSupport details;

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

		uint32_t numFormats;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &numFormats, nullptr);

		if (numFormats != 0)
		{
			details.formats.resize(numFormats);
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &numFormats, details.formats.data());
		}

		uint32_t numPresentModes;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &numPresentModes, nullptr);

		if (numPresentModes != 0)
		{
			details.presentModes.resize(numPresentModes);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &numPresentModes, details.presentModes.data());
		}

		return details;
	}

	//
	// getSwapchainSupport()
	//
	SwapchainSupport PhysicalDevice::getSwapchainSupport(VkSurfaceKHR surface) const
	{
		return _getSwapchainSupport(mDevice, surface);
	}

	//
	// getCaps()
	//
	PhysicalDeviceCaps PhysicalDevice::getCaps() const
	{
		VkPhysicalDeviceProperties properties;
		vkGetPhysicalDeviceProperties(mDevice, &properties);

		VkPhysicalDeviceFeatures features;
		vkGetPhysicalDeviceFeatures(mDevice, &features);

		// TODO: check properties.limits and features
		// ...

		PhysicalDeviceCaps caps;
		return caps;
	}

	//
	// createLogicalDevice()
	//
	VkDevice PhysicalDevice::createLogicalDevice(VkDeviceCreateInfo const& createInfo) const
	{
		VkDevice logicalDevice;
		checkResult(vkCreateDevice(mDevice, &createInfo, nullptr, &logicalDevice), "Cannot create logical device.");

		return logicalDevice;
	}

	//
	// getMemoryType()
	//
	uint32_t PhysicalDevice::getMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) const
	{
		VkPhysicalDeviceMemoryProperties memProperties;
		vkGetPhysicalDeviceMemoryProperties(mDevice, &memProperties);

		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
		{
			if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
			{
				return i;
			}
		}

		string errMsg = "Could not find a suitable memory type for filter " +
			utils::StringUtils::toString(typeFilter) + " with properties " +
			utils::StringUtils::toString(properties);

		throw VulkanException(errMsg);
	}

	//
	// getSupportedFormat()
	//
	VkFormat PhysicalDevice::getSupportedFormat(vector<VkFormat> const& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) const
	{
		for (VkFormat format : candidates)
		{
			VkFormatProperties props;
			vkGetPhysicalDeviceFormatProperties(mDevice, format, &props);

			if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
			{
				return format;
			}
			else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
			{
				return format;
			}
		}

		throw VulkanException("Could not get supported format.");
	}

} // forge