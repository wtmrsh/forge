#include "forge/UniformBuffer.h"
#include "forge/ForgeHelper.h"
#include "forge/Debug.h"

namespace forge
{

	//
	// UniformBuffer()
	//
	UniformBuffer::UniformBuffer(LogicalDevice* logicalDevice)
		: Buffer(logicalDevice)
	{
	}

	//
	// ~UniformBuffer()
	//
	UniformBuffer::~UniformBuffer()
	{
		destroy();
	}

	//
	// create()
	//
	void UniformBuffer::create(size_t uboSize, PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue)
	{
		// This releases any handles/memory if they're already created, so we essentially recreate
		// the buffer.  To improve performance, we can cache its created state.
		destroy();

		ForgeHelper::createBuffer(
			physicalDevice,
			mLogicalDevice,
			(VkDeviceSize)uboSize,
			VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			mBuffer, 
			mBufferMemory);
	}

	//
	// destroy()
	//
	void UniformBuffer::destroy()
	{
		vkDestroyBuffer(mLogicalDevice->getDevice(), mBuffer, nullptr);
		mBuffer = VK_NULL_HANDLE;

		vkFreeMemory(mLogicalDevice->getDevice(), mBufferMemory, nullptr);
		mBufferMemory = VK_NULL_HANDLE;
	}

} // forge
