#include <vector>

#include "forge/Debug.h"
#include "forge/BadResultCode.h"

namespace forge
{
	using namespace std;

	//
	// checkResult()
	//
	void checkResult(VkResult result, string const& errMsg)
	{
		if (result != VK_SUCCESS)
		{
			throw BadResultCode(result, errMsg);
		}
	}

	vector<MessageHandlerInfo> gMessageHandlers;

	//
	// message()
	//
	void message(MessageLevel level, string const& msg)
	{
		for (auto handler: gMessageHandlers)
		{
			if (handler.level >= level)
			{
				handler.handler(msg, level);
			}
		}
	}

} // forge