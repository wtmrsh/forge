#include "forge/Texture.h"
#include "forge/Buffer.h"
#include "forge/ForgeHelper.h"
#include "forge/Debug.h"
#include "forge/VulkanException.h"

namespace forge
{

	using namespace std;

	//
	// Texture()
	//
	Texture::Texture(LogicalDevice* logicalDevice, vector<char> const& data, int width, int height, int numChannels)
		: Image(logicalDevice, data, width, height, numChannels)
		, mSampler(VK_NULL_HANDLE)
	{
	}

	//
	// ~Texture()
	//
	Texture::~Texture()
	{
		vkDestroySampler(mLogicalDevice->getDevice(), mSampler, nullptr);
		mSampler = VK_NULL_HANDLE;
	}
	
	//
	// getSampler()
	//
	VkSampler Texture::getSampler() const
	{
		return mSampler;
	}

	//
	// create()
	//
	void Texture::create(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue, bool clearData)
	{
		Image::create(physicalDevice, pool, queue, clearData);

		// Create sampler
		createSampler();
	}

	//
	// createSampler()
	//
	void Texture::createSampler()
	{
		VkSamplerCreateInfo samplerInfo = {};

		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = VK_FILTER_LINEAR;
		samplerInfo.minFilter = VK_FILTER_LINEAR;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.anisotropyEnable = VK_TRUE;
		samplerInfo.maxAnisotropy = 16;
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;
		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;

		checkResult(vkCreateSampler(mLogicalDevice->getDevice(), &samplerInfo, nullptr, &mSampler), "Could not create texture sampler.");
	}

} // forge

