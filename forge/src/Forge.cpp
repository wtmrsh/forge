#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <string>
#include <vector>
#include <set>
#include <map>
#include <algorithm>

#include <glm/gtc/matrix_transform.hpp>

#include <utils/StringUtils.h>

#include "forge/Forge.h"
#include "forge/ForgeHelper.h"
#include "forge/VulkanException.h"
#include "forge/BadResultCode.h"

namespace forge
{
	using namespace std;

	extern vector<MessageHandlerInfo> gMessageHandlers;

	//
	// Forge()
	//
	Forge::Forge(string const& appName, SurfaceFactory surfaceFactory, BinaryFileLoader binaryFileLoader, ImageFileLoader imageFileLoader)
		: mAppName(appName)
		, mInstance(nullptr)
		, mPhysicalDevice(nullptr)
		, mLogicalDevice(nullptr)
		, mSurfaceFactory(surfaceFactory)
		, mUseValidationLayers(false)
		, mBinaryFileLoader(binaryFileLoader)
		, mImageFileLoader(imageFileLoader)
		, mCurrentFrame(0)
		, mWindowResized(false)
		, mDepthBuffer(nullptr)
	{
	}

	//
	// ~Forge()
	//
	Forge::~Forge()
	{
		auto logicalDevice = mLogicalDevice->getDevice();

		vkDeviceWaitIdle(logicalDevice);

		// Destroy swapchain
		destroySwapchain();

		// Destroy descriptor pool
		message(MessageLevel::Info, "Destroying descriptor pool.");
		vkDestroyDescriptorPool(logicalDevice, mDescriptorPool, nullptr);

		// Destroy descriptor sets
		message(MessageLevel::Info, "Destroying descriptor set layout.");
		vkDestroyDescriptorSetLayout(logicalDevice, mDescriptorSetLayout, nullptr);

		message(MessageLevel::Info, "Destroying uniform buffers.");
		for (auto mvpBuffer: mMvpBuffers) 
		{
			message(MessageLevel::Info, "- Destroying MVP uniform buffer.");
			delete mvpBuffer;
		}

		// Destroy vertex buffers
		message(MessageLevel::Info, "Destroying vertex buffers.");
		for (auto vertexBuffer: mVertexBuffers)
		{
			delete vertexBuffer;
		}

		// Destroy textures
		message(MessageLevel::Info, "Destroying textures.");
		for (auto texture: mTextures)
		{
			delete texture;
		}

		// Destroy semaphores and fences
		message(MessageLevel::Info, "Destroying fences.");
		for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
		{
			message(MessageLevel::Info, "- Destroying fence.");
			vkDestroyFence(logicalDevice, mInFlightFences[i], nullptr);
		}

		message(MessageLevel::Info, "Destroying semaphore sets.");
		for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
		{
			message(MessageLevel::Info, "- Destroying semaphore set.");
			vkDestroySemaphore(logicalDevice, mRenderFinishedSemaphores[i], nullptr);
			vkDestroySemaphore(logicalDevice, mImageAvailableSemaphores[i], nullptr);
		}

		// Destroy command pools
		message(MessageLevel::Info, "Destroying command pool.");
		vkDestroyCommandPool(logicalDevice, mCommandPool, nullptr);

		// Destroy logical device
		message(MessageLevel::Info, "Destroying logical device.");
		delete mLogicalDevice;
		mLogicalDevice = nullptr;

		// Destroy debug messenger
		if (mUseValidationLayers)
		{
			mInstance->destroyDebugMessenger();
		}

		// Destroy physical device
		message(MessageLevel::Info, "Destroying logical device.");
		delete mPhysicalDevice;
		mPhysicalDevice = nullptr;

		// Destroy surface
		message(MessageLevel::Info, "Destroying surface.");
		mInstance->destroySurface(&mSurface);

		// Destroy instance
		message(MessageLevel::Info, "Destroying instance.");
		delete mInstance;
		mInstance = nullptr;
	}

	//
	// initialise()
	//
	void Forge::initialise(int windowWidth, int windowHeight, vector<char const*> const& requiredExtensions, bool enableValidationLayers)
	{
		mWindowWidth = windowWidth;
		mWindowHeight = windowHeight;
		mWindowResized = false;
		mUseValidationLayers = enableValidationLayers;

		// Add the debug callback extension if we're using validation layers
		auto requiredExtensionsWithLayers = requiredExtensions;
		if (mUseValidationLayers)
		{
			requiredExtensionsWithLayers.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}

		// Set validation layers
		vector<const char*> validationLayers = {
			"VK_LAYER_LUNARG_standard_validation"
		};

		// Set up instance paramaetrs and create
		InstanceParameters instanceParams;

		instanceParams.applicationName = mAppName;
		instanceParams.applicationVersionMajor = 1;
		instanceParams.applicationVersionMinor = 0;
		instanceParams.applicationVersionPatch = 0;

		instanceParams.engineName = FORGE_NAME;
		instanceParams.engineVersionMajor = 1;
		instanceParams.engineVersionMinor = 0;
		instanceParams.engineVersionPatch = 0;

		instanceParams.requiredExtensions = requiredExtensionsWithLayers;
		instanceParams.requiredValidationLayers = validationLayers;

		instanceParams.debugMessageSeverities = 
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

		instanceParams.debugMessageTypes = 
			VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

		mInstance = new Instance(instanceParams);

		// Set up validation layer message handler
		if (mUseValidationLayers)
		{
			mInstance->createDebugMessenger();
		}

		// Create surface to render to
		mSurface = mInstance->createSurface(mSurfaceFactory);

		// Choose a physical device to use
		vector<char const*> deviceExtensions = {
			VK_KHR_SWAPCHAIN_EXTENSION_NAME
		};

		PhysicalDeviceParameters physicalDeviceParams;

		physicalDeviceParams.requiredExtensions = deviceExtensions;

		mPhysicalDevice = new PhysicalDevice(mInstance->getPhysicalDevices(), mSurface, physicalDeviceParams);

		// Set up logical device and create
		LogicalDeviceParameters logicalDeviceParams;

		logicalDeviceParams.requiredExtensions = deviceExtensions;
		logicalDeviceParams.requiredValidationLayers = validationLayers;

		mLogicalDevice = new LogicalDevice(*mPhysicalDevice, mSurface, logicalDeviceParams);

		QueueFamilyIndices indices = mPhysicalDevice->getQueueFamilies(mSurface);
		vkGetDeviceQueue(mLogicalDevice->getDevice(), indices.graphicsFamily.value(), 0, &mGraphicsQueue);
		vkGetDeviceQueue(mLogicalDevice->getDevice(), indices.presentFamily.value(), 0, &mPresentQueue);

		// Create the swap chain and swapchain images
		createSwapchain();

		// Create image views
		createImageViews();

		// Create render passes
		createRenderPasses();

		// Create descriptor sets
		createDescriptorSetLayout();

		// Create graphics pipelines
		createPipelines();

		// Create command pools
		createCommandPools();

		// Create depth buffer
		message(MessageLevel::Info, "Creating depth buffer.");
		mDepthBuffer = new DepthBuffer(mLogicalDevice, mWindowWidth, mWindowHeight);
		mDepthBuffer->create(mPhysicalDevice, mCommandPool, mGraphicsQueue);

		// Create framebuffers
		createFramebuffers();

		// Create vertex buffers
		createVertexBuffer();

		// Create texture
		createTexture();

		// Create uniforms & descriptors
		createUniformBuffers();

		createDescriptorPool();

		createDescriptorSets();

		// Create command buffer
		createCommandBuffers();

		// Create semaphores for rendering
		createSyncObjects();
	}

	//
	// resizeWindow()
	//
	void Forge::resizeWindow(int windowWidth, int windowHeight)
	{
		mWindowWidth = windowWidth;
		mWindowHeight = windowHeight;
		mWindowResized = true;
	}

	//
	// createSwapchain()
	//
	void Forge::createSwapchain() 
	{
		message(MessageLevel::Info, "Creating swapchain.");

		SwapchainSupport swapchainSupport = mPhysicalDevice->getSwapchainSupport(mSurface);

		VkSurfaceFormatKHR surfaceFormat = getSwapSurfaceFormat(swapchainSupport.formats);
		// TODO: print chosen format
		// ...

		VkPresentModeKHR presentMode = getSwapPresentMode(swapchainSupport.presentModes);
		// TODO: print chosen mode
		// ...

		VkExtent2D extent = getSwapExtent(swapchainSupport.capabilities);
		message(MessageLevel::Info, "- Width: " + utils::StringUtils::toString(extent.width));
		message(MessageLevel::Info, "- Height: " + utils::StringUtils::toString(extent.height));

		uint32_t numImages = swapchainSupport.capabilities.minImageCount + 1;
		if (swapchainSupport.capabilities.maxImageCount > 0 && numImages > swapchainSupport.capabilities.maxImageCount) 
		{
			numImages = swapchainSupport.capabilities.maxImageCount;
		}

		message(MessageLevel::Info, "- Using " + utils::StringUtils::toString(numImages) + " images");

		VkSwapchainCreateInfoKHR createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.surface = mSurface;

		createInfo.minImageCount = numImages;
		createInfo.imageFormat = surfaceFormat.format;
		createInfo.imageColorSpace = surfaceFormat.colorSpace;
		createInfo.imageExtent = extent;
		createInfo.imageArrayLayers = 1;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

		QueueFamilyIndices indices = mPhysicalDevice->getQueueFamilies(mSurface);
		uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };

		if (indices.graphicsFamily != indices.presentFamily) 
		{
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		}
		else 
		{
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		}

		// TODO: pass in as option the preTransform
		// - check supportedTransforms in capabilities
		createInfo.preTransform = swapchainSupport.capabilities.currentTransform;

		// TODO: pass in as option the compositeAlpha
		// - check other options and what they do
		createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		createInfo.presentMode = presentMode;

		// TODO: pass in as option the clipped
		// - this says don't worry about pixels hidden under other windows
		createInfo.clipped = VK_TRUE;

		createInfo.oldSwapchain = VK_NULL_HANDLE;

		auto logicalDevice = mLogicalDevice->getDevice();
		checkResult(vkCreateSwapchainKHR(logicalDevice, &createInfo, nullptr, &mSwapchain), "Cannot create swapchain.");

		vkGetSwapchainImagesKHR(logicalDevice, mSwapchain, &numImages, nullptr);
		mSwapchainImages.resize(numImages);
		vkGetSwapchainImagesKHR(logicalDevice, mSwapchain, &numImages, mSwapchainImages.data());

		mSwapchainImageFormat = surfaceFormat.format;
		mSwapchainExtent = extent;
	}

	//
	// destroySwapchain()
	//
	void Forge::destroySwapchain()
	{
		auto logicalDevice = mLogicalDevice->getDevice();

		message(MessageLevel::Info, "Destroying depth buffer.");
		delete mDepthBuffer;
		mDepthBuffer = nullptr;

		// Destroy framebuffers
		message(MessageLevel::Info, "Destroying framebuffers.");
		for (auto framebuffer: mSwapchainFramebuffers)
		{
			message(MessageLevel::Info, "- Destroying framebuffer.");
			vkDestroyFramebuffer(logicalDevice, framebuffer, nullptr);
		}

		// Destroy command buffers
		message(MessageLevel::Info, "Destroying command buffers.");
		vkFreeCommandBuffers(logicalDevice, mCommandPool, (uint32_t)mCommandBuffers.size(), mCommandBuffers.data());

		// Destroy pipeline
		message(MessageLevel::Info, "Destroying pipeline.");
		vkDestroyPipeline(logicalDevice, mPipeline, nullptr);

		// Destroy pipeline layout
		message(MessageLevel::Info, "Destroying pipeline layout.");
		vkDestroyPipelineLayout(logicalDevice, mPipelineLayout, nullptr);

		// Destroy render pass
		message(MessageLevel::Info, "Destroying render pass.");
		vkDestroyRenderPass(logicalDevice, mRenderPass, nullptr);

		// Destroy image views
		message(MessageLevel::Info, "Destroying imageviews.");
		for (auto imageView: mSwapchainImageViews)
		{
			message(MessageLevel::Info, "- Destroying imageview.");
			vkDestroyImageView(logicalDevice, imageView, nullptr);
		}

		// Destroy swapchain
		message(MessageLevel::Info, "Destroying swpachain.");
		vkDestroySwapchainKHR(logicalDevice, mSwapchain, nullptr);
	}

	//
	// recreateSwapChain()
	//
	void Forge::recreateSwapchain() 
	{
		vkDeviceWaitIdle(mLogicalDevice->getDevice());

		destroySwapchain();
		createSwapchain();

		createImageViews();
		createRenderPasses();
		createPipelines();

		message(MessageLevel::Info, "Creating depth buffer.");
		mDepthBuffer = new DepthBuffer(mLogicalDevice, mWindowWidth, mWindowHeight);
		mDepthBuffer->create(mPhysicalDevice, mCommandPool, mGraphicsQueue);

		createFramebuffers();
		createCommandBuffers();
	}

	//
	// getSwapSurfaceFormat()
	//
	VkSurfaceFormatKHR Forge::getSwapSurfaceFormat(vector<VkSurfaceFormatKHR> const& availableFormats) 
	{
		if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) 
		{
			return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
		}

		for (auto const& availableFormat: availableFormats)
		{
			if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) 
			{
				return availableFormat;
			}
		}

		return availableFormats[0];
	}

	//
	// getSwapPresentMode()
	//
	VkPresentModeKHR Forge::getSwapPresentMode(vector<VkPresentModeKHR> const& availablePresentModes) 
	{
		VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

		for (auto const& availablePresentMode: availablePresentModes) 
		{
			if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) 
			{
				return availablePresentMode;
			}
			else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) 
			{
				bestMode = availablePresentMode;
			}
		}

		return bestMode;
	}

	//
	// getSwapExtent()
	//
	VkExtent2D Forge::getSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) 
	{
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) 
		{
			return capabilities.currentExtent;
		}
		else 
		{
			VkExtent2D actualExtent = { (uint32_t)mWindowWidth, (uint32_t)mWindowHeight };

			actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

			return actualExtent;
		}
	}

	//
	// createImageViews()
	//
	void Forge::createImageViews()
	{
		message(MessageLevel::Info, "Creating imageviews.");

		mSwapchainImageViews.resize(mSwapchainImages.size());

		for (size_t i = 0; i < mSwapchainImages.size(); i++) 
		{
			message(MessageLevel::Info, "- Creating imageview.");
			mSwapchainImageViews[i] = ForgeHelper::createImageView(mLogicalDevice, mSwapchainImages[i], mSwapchainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT);
		}
	}

	//
	// createRenderPasses()
	//
	void Forge::createRenderPasses()
	{
		message(MessageLevel::Info, "Creating render passes.");

		// Colour buffer
		VkAttachmentDescription colorAttachment = {};
		colorAttachment.format = mSwapchainImageFormat;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		// Depth buffer
		VkAttachmentDescription depthAttachment = {};
		depthAttachment.format = mDepthBuffer->getDepthFormat(mPhysicalDevice);
		depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkAttachmentReference colorAttachmentRef = {};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkAttachmentReference depthAttachmentRef = {};
		depthAttachmentRef.attachment = 1;
		depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkSubpassDescription subpass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = &colorAttachmentRef;
		subpass.pDepthStencilAttachment = &depthAttachmentRef;

		VkSubpassDependency dependency = {};
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		dependency.dstSubpass = 0;
		dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.srcAccessMask = 0;
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		array<VkAttachmentDescription, 2> attachments = { colorAttachment, depthAttachment };
		VkRenderPassCreateInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = (uint32_t)attachments.size();
		renderPassInfo.pAttachments = attachments.data();
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpass;
		renderPassInfo.dependencyCount = 1;
		renderPassInfo.pDependencies = &dependency;

		checkResult(vkCreateRenderPass(mLogicalDevice->getDevice(), &renderPassInfo, nullptr, &mRenderPass), "Could not create render pass.");
	}

	//
	// createDescriptorPool()
	//
	void Forge::createDescriptorPool()
	{
		message(MessageLevel::Info, "Creating descriptor pool.");

		array<VkDescriptorPoolSize, 2> poolSizes = {};

		poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		poolSizes[0].descriptorCount = (uint32_t)mSwapchainImages.size();
		poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolSizes[1].descriptorCount = (uint32_t)mSwapchainImages.size();

		VkDescriptorPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolInfo.poolSizeCount = (uint32_t)poolSizes.size();
		poolInfo.pPoolSizes = poolSizes.data();
		poolInfo.maxSets = (uint32_t)mSwapchainImages.size();

		checkResult(vkCreateDescriptorPool(mLogicalDevice->getDevice(), &poolInfo, nullptr, &mDescriptorPool), "Could not create descriptor pool.");
	}

	//
	// createDescriptorSetLayout()
	//
	void Forge::createDescriptorSetLayout()
	{
		message(MessageLevel::Info, "Creating descriptor set layout.");

		// Descriptor sets do not have to be recreated along with the swapchain.
		// Bindings are specified for each descriptor, eg UBOs.  Then all are added to the master layout.

		// ModelViewProjection UBO layout
		VkDescriptorSetLayoutBinding mvpLayoutBinding = {};

		mvpLayoutBinding.binding = 0;										// Binding used in shader
		mvpLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		mvpLayoutBinding.descriptorCount = 1;								// Size of array in shader (ie how many instances of this UBO)
		mvpLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;			// Bitfield to specify which shader stages it's used
		mvpLayoutBinding.pImmutableSamplers = nullptr;						// Used for descriptors which sample images.

		// Sampler layout
		VkDescriptorSetLayoutBinding samplerLayoutBinding = {};

		samplerLayoutBinding.binding = 1;
		samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		samplerLayoutBinding.descriptorCount = 1;

		samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		samplerLayoutBinding.pImmutableSamplers = nullptr;

		// Create master layout
		array<VkDescriptorSetLayoutBinding, 2> bindings = { mvpLayoutBinding, samplerLayoutBinding };

		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = (uint32_t)bindings.size();
		layoutInfo.pBindings = bindings.data();

		checkResult(vkCreateDescriptorSetLayout(mLogicalDevice->getDevice(), &layoutInfo, nullptr, &mDescriptorSetLayout), "Could not create descript set layout.");
	}

	//
	// createDescriptorSets()
	//
	void Forge::createDescriptorSets()
	{
		vector<VkDescriptorSetLayout> layouts(mSwapchainImages.size(), mDescriptorSetLayout);

		size_t numSets = mSwapchainImages.size();

		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = mDescriptorPool;
		allocInfo.descriptorSetCount = (uint32_t)numSets;
		allocInfo.pSetLayouts = layouts.data();

		mDescriptorSets.resize(numSets);
		checkResult(vkAllocateDescriptorSets(mLogicalDevice->getDevice(), &allocInfo, mDescriptorSets.data()), "Could not allocate descriptor sets.");

		for (size_t i = 0; i < numSets; i++) 
		{
			VkDescriptorBufferInfo bufferInfo = {};
			bufferInfo.buffer = mMvpBuffers[i]->getBuffer();
			bufferInfo.offset = 0;
			bufferInfo.range = sizeof(ModelViewProjection);

			VkDescriptorImageInfo imageInfo = {};
			imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageInfo.imageView = mTextures[0]->getImageView();
			imageInfo.sampler = mTextures[0]->getSampler();

			std::array<VkWriteDescriptorSet, 2> descriptorWrites = {};

			descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[0].dstSet = mDescriptorSets[i];
			descriptorWrites[0].dstBinding = 0;
			descriptorWrites[0].dstArrayElement = 0;
			descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrites[0].descriptorCount = 1;
			descriptorWrites[0].pBufferInfo = &bufferInfo;

			descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[1].dstSet = mDescriptorSets[i];
			descriptorWrites[1].dstBinding = 1;
			descriptorWrites[1].dstArrayElement = 0;
			descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			descriptorWrites[1].descriptorCount = 1;
			descriptorWrites[1].pImageInfo = &imageInfo;

			vkUpdateDescriptorSets(mLogicalDevice->getDevice(), (uint32_t)descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
		}
	}

	//
	// createUniformBuffers
	//
	void Forge::createUniformBuffers()
	{
		message(MessageLevel::Info, "Creating uniform buffers.");

		size_t mvpSize = sizeof(ModelViewProjection);

		for (size_t i = 0; i < mSwapchainImages.size(); ++i)
		{
			auto buffer = new UniformBuffer(mLogicalDevice);
			buffer->create(mvpSize, mPhysicalDevice, mCommandPool, mGraphicsQueue);

			mMvpBuffers.push_back(buffer);
		}
	}

	//
	// createPipelines()
	//
	void Forge::createPipelines()
	{
		auto logicalDevice = mLogicalDevice->getDevice();

		message(MessageLevel::Info, "Creating pipelines.");

		// Programmable (shader) section of pipeline
		VkShaderModule vertShaderModule = createShaderModule(mBinaryFileLoader("../../../resources/shaders/vert.spv"));
		VkShaderModule fragShaderModule = createShaderModule(mBinaryFileLoader("../../../resources/shaders/frag.spv"));

		// TODO: look into VkPipelineShaderStageCreateInfo.pSpecializationInfo
		// ...

		// Vertex shader
		VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
		vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
		vertShaderStageInfo.module = vertShaderModule;
		vertShaderStageInfo.pName = "main";

		// Tesselation shader
		// ...

		// Geometry shader
		// ...

		// Fragment shader
		VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
		fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		fragShaderStageInfo.module = fragShaderModule;
		fragShaderStageInfo.pName = "main";

		VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

		// Fixed function section of pipeline
		VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

		auto bindingDescription = Vertex::getBindingDescription();
		auto attributeDescriptions = Vertex::getAttributeDescriptions();

		vertexInputInfo.vertexBindingDescriptionCount = 1;
		vertexInputInfo.vertexAttributeDescriptionCount = (uint32_t)attributeDescriptions.size();
		vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
		vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

		VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
		inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		inputAssembly.primitiveRestartEnable = VK_FALSE;

		VkViewport viewport = {};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (float)mSwapchainExtent.width;
		viewport.height = (float)mSwapchainExtent.height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		VkRect2D scissor = {};
		scissor.offset = { 0, 0 };
		scissor.extent = mSwapchainExtent;

		VkPipelineViewportStateCreateInfo viewportState = {};
		viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportState.viewportCount = 1;
		viewportState.pViewports = &viewport;
		viewportState.scissorCount = 1;
		viewportState.pScissors = &scissor;

		VkPipelineRasterizationStateCreateInfo rasterizer = {};
		rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizer.depthClampEnable = VK_FALSE;
		rasterizer.rasterizerDiscardEnable = VK_FALSE;
		rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
		rasterizer.lineWidth = 1.0f;
		rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
		rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		rasterizer.depthBiasEnable = VK_FALSE;

		VkPipelineMultisampleStateCreateInfo multisampling = {};
		multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampling.sampleShadingEnable = VK_FALSE;
		multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

		VkPipelineDepthStencilStateCreateInfo depthStencil = {};
		depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthStencil.depthTestEnable = VK_TRUE;
		depthStencil.depthWriteEnable = VK_TRUE;
		depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
		depthStencil.depthBoundsTestEnable = VK_FALSE;
		depthStencil.stencilTestEnable = VK_FALSE;
		//depthStencil.minDepthBounds = 0.0f; // Optional
		//depthStencil.maxDepthBounds = 1.0f; // Optional
		//depthStencil.front = {}; // Optional
		//depthStencil.back = {}; // Optional
		VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachment.blendEnable = VK_FALSE;

		VkPipelineColorBlendStateCreateInfo colorBlending = {};
		colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlending.logicOpEnable = VK_FALSE;
		colorBlending.logicOp = VK_LOGIC_OP_COPY;
		colorBlending.attachmentCount = 1;
		colorBlending.pAttachments = &colorBlendAttachment;
		colorBlending.blendConstants[0] = 0.0f;
		colorBlending.blendConstants[1] = 0.0f;
		colorBlending.blendConstants[2] = 0.0f;
		colorBlending.blendConstants[3] = 0.0f;

		VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = 1;
		pipelineLayoutInfo.pSetLayouts = &mDescriptorSetLayout;
		pipelineLayoutInfo.pushConstantRangeCount = 0;

		checkResult(vkCreatePipelineLayout(logicalDevice, &pipelineLayoutInfo, nullptr, &mPipelineLayout), "Could not create pipeline layout.");

		// Create pipeline from layout
		VkGraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineInfo.stageCount = 2;
		pipelineInfo.pStages = shaderStages;
		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &inputAssembly;
		pipelineInfo.pViewportState = &viewportState;
		pipelineInfo.pRasterizationState = &rasterizer;
		pipelineInfo.pMultisampleState = &multisampling;
		pipelineInfo.pDepthStencilState = &depthStencil;
		pipelineInfo.pColorBlendState = &colorBlending;
		pipelineInfo.layout = mPipelineLayout;
		pipelineInfo.renderPass = mRenderPass;
		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

		checkResult(vkCreateGraphicsPipelines(logicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &mPipeline), "Could not create pipeline.");

		// Cleanup
		vkDestroyShaderModule(logicalDevice, fragShaderModule, nullptr);
		vkDestroyShaderModule(logicalDevice, vertShaderModule, nullptr);
	}

	//
	// createShaderModule()
	//
	VkShaderModule Forge::createShaderModule(vector<char> const& bytecode)
	{
		VkShaderModuleCreateInfo createInfo = {};
			
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = bytecode.size();
		createInfo.pCode = (uint32_t const*)bytecode.data();

		VkShaderModule shaderModule;
		checkResult(vkCreateShaderModule(mLogicalDevice->getDevice(), &createInfo, nullptr, &shaderModule), "Could not create shader module.");

		return shaderModule;
	}

	//
	// createFramebuffers
	//
	void Forge::createFramebuffers()
	{
		message(MessageLevel::Info, "Creating framebuffers.");

		mSwapchainFramebuffers.resize(mSwapchainImageViews.size());

		for (size_t i = 0; i < mSwapchainImageViews.size(); i++) 
		{
			message(MessageLevel::Info, "- Creating framebuffer.");

			array<VkImageView, 2> attachments = {
				mSwapchainImageViews[i],
				mDepthBuffer->getImageView()
			};

			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = mRenderPass;
			framebufferInfo.attachmentCount = (uint32_t)attachments.size();
			framebufferInfo.pAttachments = attachments.data();
			framebufferInfo.width = mSwapchainExtent.width;
			framebufferInfo.height = mSwapchainExtent.height;
			framebufferInfo.layers = 1;

			checkResult(vkCreateFramebuffer(mLogicalDevice->getDevice(), &framebufferInfo, nullptr, &mSwapchainFramebuffers[i]), "Could not create framebuffer.");
		}
	}

	//
	// createCommandPools()
	//
	void Forge::createCommandPools()
	{
		message(MessageLevel::Info, "Creating command pools.");

		QueueFamilyIndices queueFamilyIndices = mPhysicalDevice->getQueueFamilies(mSurface);

		VkCommandPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
		//poolInfo.flags |= VK_COMMAND_POOL_CREATE_TRANSIENT_BIT; // Hint that command buffers will be updated often
		//poolInfo.flags |= VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Command buffers can be rerecorded individually

		checkResult(vkCreateCommandPool(mLogicalDevice->getDevice(), &poolInfo, nullptr, &mCommandPool), "Could not create command pool.");
	}

	//
	// createCommandBuffers()
	//
	void Forge::createCommandBuffers()
	{
		message(MessageLevel::Info, "Creating command buffers.");

		mCommandBuffers.resize(mSwapchainFramebuffers.size());

		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandPool = mCommandPool;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandBufferCount = (uint32_t)mCommandBuffers.size();

		checkResult(vkAllocateCommandBuffers(mLogicalDevice->getDevice(), &allocInfo, mCommandBuffers.data()), "Could not allocate command buffers.");

		for (size_t i = 0; i < mCommandBuffers.size(); i++) 
		{
			message(MessageLevel::Info, "- Creating command buffer.");

			VkCommandBufferBeginInfo beginInfo = {};
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

			checkResult(vkBeginCommandBuffer(mCommandBuffers[i], &beginInfo), "Failed to begin recording command buffer.");

			VkRenderPassBeginInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderPassInfo.renderPass = mRenderPass;
			renderPassInfo.framebuffer = mSwapchainFramebuffers[i];
			renderPassInfo.renderArea.offset = { 0, 0 };
			renderPassInfo.renderArea.extent = mSwapchainExtent;


			array<VkClearValue, 2> clearValues = {};
			clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
			clearValues[1].depthStencil = { 1.0f, 0 };

			renderPassInfo.clearValueCount = (uint32_t)clearValues.size();
			renderPassInfo.pClearValues = clearValues.data();

			vkCmdBeginRenderPass(mCommandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

			vkCmdBindPipeline(mCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, mPipeline);

			// Draw vertex buffers
			for (auto vertexBuffer: mVertexBuffers)
			{
				VkBuffer vertexBuffers[] = { vertexBuffer->getBuffer() };
				VkDeviceSize offsets[] = { 0 };

				vkCmdBindVertexBuffers(mCommandBuffers[i], 0, 1, vertexBuffers, offsets);
				vkCmdBindIndexBuffer(mCommandBuffers[i], vertexBuffer->getIndexBuffer(), 0, VK_INDEX_TYPE_UINT16);

				vkCmdBindDescriptorSets(mCommandBuffers[i],	VK_PIPELINE_BIND_POINT_GRAPHICS, mPipelineLayout, 0, 1, &mDescriptorSets[i], 0, nullptr);
				//vkCmdDraw(mCommandBuffers[i], mVertBuffer->getNumVertices(), 1, 0, 0);
				vkCmdDrawIndexed(mCommandBuffers[i], vertexBuffer->getNumIndices(), 1, 0, 0, 0);
			}

			vkCmdEndRenderPass(mCommandBuffers[i]);

			checkResult(vkEndCommandBuffer(mCommandBuffers[i]), "Failed to record command buffer.");
		}
	}

	//
	// createSyncObjects()
	//
	void Forge::createSyncObjects()
	{
		// Semaphores
		message(MessageLevel::Info, "Creating semaphore sets.");

		mImageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
		mRenderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);

		VkSemaphoreCreateInfo semaphoreInfo = {};
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
		{
			message(MessageLevel::Info, "- Creating semaphore set.");
			checkResult(vkCreateSemaphore(mLogicalDevice->getDevice(), &semaphoreInfo, nullptr, &mImageAvailableSemaphores[i]), "Could not create semaphore.");
			checkResult(vkCreateSemaphore(mLogicalDevice->getDevice(), &semaphoreInfo, nullptr, &mRenderFinishedSemaphores[i]), "Could not create semaphore.");
		}

		// Fences
		message(MessageLevel::Info, "Creating fences.");

		mInFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

		VkFenceCreateInfo fenceInfo = {};
		fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

		for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
		{
			message(MessageLevel::Info, "- Creating fence.");
			checkResult(vkCreateFence(mLogicalDevice->getDevice(), &fenceInfo, nullptr, &mInFlightFences[i]), "Could not create fence.");
		}
	}

	//
	// checkResult()
	//
	void Forge::checkResult(VkResult result, string const& errMsg)
	{
		if (result != VK_SUCCESS)
		{
			throw BadResultCode(result, errMsg);
		}
	}

	//
	// addMessageHandler()
	//
	void Forge::addMessageHandler(MessageHandler handler, MessageLevel level)
	{
		MessageHandlerInfo info{ handler, level };
		gMessageHandlers.push_back(info);
	}

	//
	// createVertexBuffer()
	//
	VertexBuffer* Forge::createVertexBuffer()
	{
		auto vertexBuffer = new VertexBuffer(mLogicalDevice, true);
		vertexBuffer->create(mPhysicalDevice, mCommandPool, mGraphicsQueue);

		mVertexBuffers.push_back(vertexBuffer);
		return vertexBuffer;
	}

	//
	// createTexture
	//
	Texture* Forge::createTexture()
	{
		vector<char> data = mImageFileLoader("../../../resources/images/greystone.jpg");

		auto texture = new Texture(mLogicalDevice, data, 512, 512, 3);
		texture->create(mPhysicalDevice, mCommandPool, mGraphicsQueue, true);

		mTextures.push_back(texture);
		return texture;
	}

	//
	// renderFrame()
	//
	void Forge::renderFrame()
	{
		vkWaitForFences(mLogicalDevice->getDevice(), 1, &mInFlightFences[mCurrentFrame],	VK_TRUE, numeric_limits<uint64_t>::max());

		// Get next image
		uint32_t imageIndex;
		auto result = vkAcquireNextImageKHR(mLogicalDevice->getDevice(), mSwapchain, numeric_limits<uint64_t>::max(), mImageAvailableSemaphores[mCurrentFrame], VK_NULL_HANDLE, &imageIndex);

		if (result == VK_ERROR_OUT_OF_DATE_KHR || mWindowResized) 
		{
			recreateSwapchain();
			mWindowResized = false;
			return;
		}
		else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) 
		{
			throw BadResultCode(result, "Could not acquire swapchain image.");
		}

		// Update MVP
		ModelViewProjection mvp = {};
		mvp.model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		mvp.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f),	glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		mvp.proj = glm::perspective(glm::radians(45.0f), mSwapchainExtent.width / (float)mSwapchainExtent.height, 0.1f, 10.0f);

		mvp.proj[1][1] *= -1; // Correct for GLM's OpenGL bias, where y coordinate of clip axes is inverted.

		void* data;
		vkMapMemory(mLogicalDevice->getDevice(), mMvpBuffers[imageIndex]->getBufferMemory(), 0, sizeof(mvp), 0, &data);
		memcpy(data, &mvp, sizeof(mvp));
		vkUnmapMemory(mLogicalDevice->getDevice(), mMvpBuffers[imageIndex]->getBufferMemory());

		// Submit
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

		VkSemaphore waitSemaphores[] = { mImageAvailableSemaphores[mCurrentFrame] };
		VkPipelineStageFlags waitStages[] =
		{ 
			VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT 
		};
		
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;

		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &mCommandBuffers[imageIndex];

		VkSemaphore signalSemaphores[] = { mRenderFinishedSemaphores[mCurrentFrame] };
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalSemaphores;

		vkResetFences(mLogicalDevice->getDevice(), 1, &mInFlightFences[mCurrentFrame]);
		checkResult(vkQueueSubmit(mGraphicsQueue, 1, &submitInfo, mInFlightFences[mCurrentFrame]), "Could not submit draw command buffer.");

		// Presentation
		VkPresentInfoKHR presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = signalSemaphores;

		VkSwapchainKHR swapChains[] = { mSwapchain };
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = swapChains;

		presentInfo.pImageIndices = &imageIndex;

		vkQueuePresentKHR(mPresentQueue, &presentInfo);

		// Next frame
		mCurrentFrame = (mCurrentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
	}

} // forge