#pragma once

#include <vector>

#include "forge/Platform.h"

namespace forge
{

	/**
	 * Struct for storing LogicalDevice settings.
	 */
	struct LogicalDeviceParameters
	{
		// Vulkan extensions that are required.
		std::vector<char const*> requiredExtensions;

		// Validation layers required.  If this is empty, then validation layers
		// are essentially disabled.
		std::vector<char const*> requiredValidationLayers;
	};

} // forge