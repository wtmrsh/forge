#pragma once

#include <vector>

#include "forge/Platform.h"

namespace forge
{

	/**
	 * Struct for storing Instance settings.
	 */
	struct InstanceParameters
	{
		// Vulkan extensions that are required.
		std::vector<char const*> requiredExtensions;

		// Validation layers required.  If this is empty, then validation layers
		// are essentially disabled.
		std::vector<char const*> requiredValidationLayers;

		// Application name
		std::string applicationName;

		// Application version
		int applicationVersionMajor, applicationVersionMinor, applicationVersionPatch;

		// Engine name
		std::string engineName;

		// Engine version
		int engineVersionMajor, engineVersionMinor, engineVersionPatch;

		// Debug messages
		VkDebugUtilsMessageSeverityFlagsEXT debugMessageSeverities;

		VkDebugUtilsMessageTypeFlagsEXT debugMessageTypes;
	};

} // forge