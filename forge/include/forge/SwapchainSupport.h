#pragma once
#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"

namespace forge
{

	/**
	 * Struct for storing swapchain information.
	 */
	struct SwapchainSupport
	{
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	};

} // forge