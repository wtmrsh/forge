#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/Image.h"

namespace forge
{

	/**
	 * Represents a texture.
	 */
	class Texture : public Image
	{
		// Sampler
		VkSampler mSampler;

	private:

		/**
		 * Create sampler for use by shaders.
		 */
		void createSampler();

	public:

		/**
		 * Constructor.
		 *
		 * @param logicalDevice Logical device to use.
		 * @param data Decoded image data, interleaved.
		 * @param width Image width.
		 * @param height Image height.
		 * @param numChannels Number of channels.
		 *
		 */
		Texture(LogicalDevice* logicalDevice, std::vector<char> const& data, int width, int height, int numChannels);

		/**
		 * Destructor.
		 */
		virtual ~Texture();

		/**
		 * Get sampler handle.
		 *
		 * @return Sampler handle.
		 */
		VkSampler getSampler() const;

		/**
		 * Create and load the image data into a texture.
		 *
		 * @param physicalDevice Physical device to use.
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 * @param clearData Whether or not to clear out the source data once created.
		 *        Set to true if you don't need to recreate the texture
		 */
		void create(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue, bool clearData);

	};

} // forge
