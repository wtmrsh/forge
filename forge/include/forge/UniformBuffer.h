#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/Buffer.h"

namespace forge
{

	/**
	 * Represents a uniform buffer.
	 */
	class UniformBuffer : public Buffer
	{
	public:

		/**
		 * Constructor.
		 *
		  * @param logicalDevice Logical device.
		 */
		explicit UniformBuffer(LogicalDevice* logicalDevice);

		/**
		 * Destructor.
		 */
		virtual ~UniformBuffer();

		/**
		 * Create the buffer objects and fill with data.
		 *
		 * @uboSize Size of uniform buffer object, in bytes.
		 * @param physicalDevice Physical device.
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 */
		void create(size_t uboSize, PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue);

		/**
		 * Destroy the buffer objects.
		 */
		void destroy();

	};

} // forge
