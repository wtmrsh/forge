#pragma once

#include <functional>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"

namespace forge
{

	typedef std::function<VkResult(VkInstance const&, VkSurfaceKHR*)> SurfaceFactory;

} // forge