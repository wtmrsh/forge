#pragma once

#include <array>

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

#include "forge/Platform.h"

namespace forge
{

	/*
	 * Basic vertex structure.
	 */
	struct Vertex
	{
		glm::vec3 position;
		glm::vec2 uv;
		glm::vec3 colour;

		/*
		 * Return the type of vertex binding.
		 *
		 * @return Binding description.
		 */
		static VkVertexInputBindingDescription getBindingDescription()
		{
			VkVertexInputBindingDescription bindingDescription = {};
			bindingDescription.binding = 0;
			bindingDescription.stride = sizeof(Vertex);

			// VK_VERTEX_INPUT_RATE_VERTEX: move to next entry after each vertex
			// VK_VERTEX_INPUT_RATE_INSTANCE: move to next entry after each instance (for instanced rendering)
			bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

			return bindingDescription;
		}

		/*
		 * Specify the attribute layout.
		 *
		 * @return Attribute layout.
		 */
		static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions()
		{
			std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions = {};

			attributeDescriptions[0].binding = 0;
			attributeDescriptions[0].location = 0;
			attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
			attributeDescriptions[0].offset = offsetof(Vertex, position);

			attributeDescriptions[1].binding = 0;
			attributeDescriptions[1].location = 1;
			attributeDescriptions[1].format = VK_FORMAT_R32G32_SFLOAT;
			attributeDescriptions[1].offset = offsetof(Vertex, uv);

			attributeDescriptions[2].binding = 0;
			attributeDescriptions[2].location = 2;
			attributeDescriptions[2].format = VK_FORMAT_R32G32B32_SFLOAT;
			attributeDescriptions[2].offset = offsetof(Vertex, colour);

			return attributeDescriptions;
		}
	};

} // forge

