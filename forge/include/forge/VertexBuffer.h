#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/Buffer.h"
#include "forge/PhysicalDevice.h"
#include "forge/LogicalDevice.h"
#include "forge/Vertex.h"

namespace forge
{

	/**
	 * Represents a vertex buffer.  This contains the packed vertex attribute data which is sent to the
	 * shader pipeline.  It may be indexed, or unindexed.
	 */
	class VertexBuffer : public Buffer
	{
		bool mIndexed;

		VkBuffer mIndexBuffer;

		VkDeviceMemory mIndexBufferMemory;

		std::vector<Vertex> mVertices;

		std::vector<uint16_t> mIndices;

	private:

		/**
		 * Create the main data buffer.  This is a helper function for create().
		 *
		 * @param physicalDevice Physical device.
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 */
		void createDataBuffer(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue);

		/**
		 * Create the index buffer.  This is a helper function for create().
		 *
		 * @param physicalDevice Physical device.
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 */
		void createIndexBuffer(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue);

	public:

		/**
		 * Constructor.
		 *
		 * @param logicalDevice Logical device.
		 * @param indexed Specify whether the VertexBuffer is indexed or not.
		 */
		VertexBuffer(LogicalDevice* logicalDevice, bool indexed);

		/**
		 * Destructor.
		 */
		virtual ~VertexBuffer();

		/**
		 * Query whether the VertexBuffer is indexed or not.
		 *
		 * @return Whether the VertexBuffer is indexed or not.
		 */
		bool isIndexed() const;

		/*
		 * Get data buffer handle.
		 *
		 * @return Data buffer handle
		 */
		VkBuffer getIndexBuffer() const;

		/**
		 * Get vertex count.
		 *
		 * @return Number of vertices.
		 */
		uint32_t getNumVertices() const;

		/**
		 * Get index count.
		 *
		 * @return Number of indices.
		 */
		uint32_t getNumIndices() const;

		/**
		 * Create the buffer objects and fill with data.
		 *
		 * @param physicalDevice Physical device.
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 */
		void create(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue);

		/**
		 * Destroy the buffer objects.
		 */
		void destroy();

	};

} // forge
