#pragma once
#include <optional>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"

namespace forge
{

	/**
	 * Struct for storing queue family index data.
	 */
	struct QueueFamilyIndices
	{
		std::optional<uint32_t> graphicsFamily;
		std::optional<uint32_t> presentFamily;

		/**
		 * Check to see if a queue family has all the required queues.
		 *
		 * @return Whether or not a family has both graphics and presentation queues.
		 */
		bool isComplete() const
		{
			return
				graphicsFamily.has_value() &&
				presentFamily.has_value();
		}
	};

} // forge