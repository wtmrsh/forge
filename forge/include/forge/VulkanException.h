#pragma once

#include <stdexcept>
#include <string>

#include <vulkan/vulkan.h>

#include "Platform.h"

/**
 * Exception handler for vulkan error codes.
 */
class VulkanException : public std::runtime_error
{
public:

	/**
	 * Constructor.
	 *
	 * @param msg Error mesage to store.
	 */
	explicit VulkanException(std::string const& msg)
		: std::runtime_error(msg.c_str())
	{
	}
};
