#pragma once

#include <functional>
#include <optional>
#include <array>

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

#include "forge/Platform.h"
#include "forge/SurfaceFactory.h"
#include "forge/Instance.h"
#include "forge/PhysicalDevice.h"
#include "forge/LogicalDevice.h"
#include "forge/Debug.h"

#include "forge/VertexBuffer.h"
#include "forge/UniformBuffer.h"
#include "forge/DepthBuffer.h"
#include "forge/Texture.h"

namespace forge
{

	/**
	 * Binary file loader.
	 */
	typedef std::function<std::vector<char>(std::string const&)> BinaryFileLoader;

	/**
	 * Image file loader.
	 */
	typedef std::function<std::vector<char>(std::string const&)> ImageFileLoader;

	/*
	 * Hold an MVP transform.
	 */
	struct ModelViewProjection
	{
		glm::mat4 model;
		glm::mat4 view;
		glm::mat4 proj;
	};

	/**
	 * Facade class for initializing Forge.  There should be exactly one
	 * instance of this class instantiated in a program.
	 */
	class _FORGEAPI Forge
	{
		std::string mAppName;

		int mWindowWidth, mWindowHeight;

		// Instance
		Instance* mInstance;

		// Physical device
		PhysicalDevice* mPhysicalDevice;

		// Logical device
		LogicalDevice* mLogicalDevice;

		// Queues
		VkQueue mGraphicsQueue, mPresentQueue;

		// Surface
		SurfaceFactory mSurfaceFactory;

		VkSurfaceKHR mSurface;

		// Swapchain
		VkSwapchainKHR mSwapchain;

		std::vector<VkImage> mSwapchainImages;

		VkFormat mSwapchainImageFormat;

		VkExtent2D mSwapchainExtent;

		// ImageViews
		std::vector<VkImageView> mSwapchainImageViews;

		// Render pass
		VkRenderPass mRenderPass;

		// Descriptors
		VkDescriptorPool mDescriptorPool;

		VkDescriptorSetLayout mDescriptorSetLayout;

		std::vector<VkDescriptorSet> mDescriptorSets;
		// Pipeline
		VkPipelineLayout mPipelineLayout;

		VkPipeline mPipeline;

		// Framebuffers
		std::vector<VkFramebuffer> mSwapchainFramebuffers;

		// Command pools
		VkCommandPool mCommandPool;

		std::vector<VkCommandBuffer> mCommandBuffers;

		// Rendering
		static const int MAX_FRAMES_IN_FLIGHT = 2;

		std::vector<VkSemaphore> mImageAvailableSemaphores, mRenderFinishedSemaphores;

		std::vector<VkFence> mInFlightFences;

		uint32_t mCurrentFrame;

		bool mWindowResized;

		// Uniforms
		std::vector<UniformBuffer*> mMvpBuffers;

		// Vertex buffers
		std::vector<VertexBuffer*> mVertexBuffers;

		// Textures
		std::vector<Texture*> mTextures;

		// Depth buffer
		DepthBuffer* mDepthBuffer;

		// Validation and debug output
		bool mUseValidationLayers;

		// I/O
		BinaryFileLoader mBinaryFileLoader;

		ImageFileLoader mImageFileLoader;

	private:
	
		/**
		 * Checks a Vulkan function call result.  Throws
		 * if not success.
		 *
		 * @param result The result code to check.
		 * @param errMsg An additional message to print on error.
		 */
		void checkResult(VkResult result, std::string const& errMsg);

		//
		// Swapchain
		//

		/**
		 * Create the swapchain.
		 */
		void createSwapchain();

		/**
		 * Destroy the swapchain.
		 */
		void destroySwapchain();

		/**
		 * Recreate the swapchain, eg after a resize.
		 */
		void recreateSwapchain();

		/**
		 * Choose which colour space and format to use.
		 *
		 * @param availableFormats Formats to choose from.
		 */
		VkSurfaceFormatKHR getSwapSurfaceFormat(std::vector<VkSurfaceFormatKHR> const& availableFormats);

		/**
		 * Choose which presentation mode to use.
		 *
		 * @param availablePresentModes Modes to choose from.
		 */		
		VkPresentModeKHR getSwapPresentMode(std::vector<VkPresentModeKHR> const& availablePresentModes);

		/**
		 * Define the swap extents.
		 *
		 * @param capabilities Surface capabilities.
		 */
		VkExtent2D getSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

		//
		// ImageViews
		//
		
		/**
		 * Create ImageViews
		 */
		void createImageViews();

		//
		// Render passes
		//

		/**
		 * Create render pass(es)
		 */
		void createRenderPasses();

		//
		// Descriptor sets
		//

		/**
		 * Create descriptor pool
		 */
		void createDescriptorPool();

		/**
		 * Create descriptor set layout
		 */
		void createDescriptorSetLayout();

		/**
		 * Create descriptor sets
		 */
		void createDescriptorSets();

		/**
		 * Create uniform buffer objects
		 */
		void createUniformBuffers();

		//
		// Graphics pipeline
		//

		/**
		 * Create pipeline(s).
		 */
		void createPipelines();

		/**
		 * Create a shader module for a pipeline.
		 * 
		 * @param bytecode SPIR-V bytecode.
		 *
		 * @return VkShaderModule object.
		 */
		VkShaderModule createShaderModule(std::vector<char> const& bytecode);

		//
		// Framebuffers
		//

		/**
		 * Create framebuffer(s)
		 */
		void createFramebuffers();

		//
		// Command pools
		//

		/**
		 * Create command pool(s)
		 */
		void createCommandPools();

		/**
		 * Create command buffer(s)
		 */
		void createCommandBuffers();

		//
		// Rendering
 		//

		/**
		 * Create semaphores to control rendering.
		 */
		void createSyncObjects();

	public:

		/**
		 * Constructor.
		 *
		 * @param appName Name of client application, to be passed onto vulkan.
		 * @param surfaceFactory creation method for Surface.
		 * @param binaryFileLoader callback for loading binary resource files.
		 * @param imageFileLoader callback for loading image files.
		 */
		Forge(std::string const& appName, SurfaceFactory surfaceFactory, BinaryFileLoader binaryFileLoader, ImageFileLoader imageFileLoader);

		/**
		 * Destructor.
		 */		
		virtual ~Forge();

		/**
		 * Initialise vulkan.
		 *
		 * @param windowWidth Width of render window.
		 * @param windowHeight Height of render window.
		 * @param requiredExtensions Extensions needed by the instance.
		 * @param numRequiredExtensions The number of extensions passed in first parameter.
		 * @param enableValidationLayers Whether to enable validation layers or not.
		 */
		void initialise(int windowWidth, int windowHeight, std::vector<char const*> const& requiredExtensions, bool enableValidationLayers);

		/*
		 * Change window size
		 *
		 * @param windowWidth New window width.
		 * @param windowHeight New window height.
		 */
		void resizeWindow(int windowWidth, int windowHeight);

		/**
		 * Adds a message handler, to be used a logging mechanism.
		 *
		 * @param handler Message handling function.
		 * @param level Message level to listen for.
		 */
		void addMessageHandler(MessageHandler handler, MessageLevel level);

		/*
		 * Create a vertex buffer.
		 *
		 * @return Created vertex buffer.
		 */
		VertexBuffer* createVertexBuffer();

		/**
		 * Create a texture.
		 *
		 * @return Created texture.
		 */
		Texture* createTexture();

		/**
		 * Render a frame.
		 */
		void renderFrame();
	};

} // forge