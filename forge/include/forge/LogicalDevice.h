#pragma once
#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/PhysicalDevice.h"
#include "forge/LogicalDeviceParameters.h"

namespace forge
{

	/**
	 * Encapsulate a VkDevice.  This is what is used in the majority of operations.
	 */
	class LogicalDevice
	{
		// Creation parameters
		LogicalDeviceParameters mParameters;

		// Device handle.
		VkDevice mDevice;

	private:

		/**
		 * Create the logical device.  This assumes that the requested extensions and validation layers
		 * have already been checked for availability.
		 *
		 * @param physicalDevice Physical device to create with.
		 * @param surface Surface handle.
		 */
		void create(PhysicalDevice const& physicalDevice, VkSurfaceKHR surface);

	public:

		/**
		 * Constructor.
		 *
		 * @param physicalDevice Physical device to create with.
		 * @param surface Surface handle.
		 * @param parameters Parameters to create logical device with.
		 */
		LogicalDevice(PhysicalDevice const& physicalDevice, VkSurfaceKHR surface, LogicalDeviceParameters const& parameters);

		/*
		 * Destructor.
		 */
		virtual ~LogicalDevice();

		/**
		 * Get the device handle.
		 *
		 * @return Device handle.
		 */
		VkDevice getDevice() const;

	};

} // forge