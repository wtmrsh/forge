#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/PhysicalDevice.h"
#include "forge/LogicalDevice.h"

namespace forge
{

	/**
	 * Helper class for Forge.
	 */
	class ForgeHelper
	{
	public:

		/*
		 * Helper function to create a buffer of the specified type.  It returns a
		 * VkBuffer object and the memory which backs it, via out parameters.
		 *
		 * @param physicalDevice Physical device.
		 * @param logicalDevice Logical device.
		 * @param size Size of buffer in bytes.
		 * @param usage Buffer usage.
		 * @param properties Memory properties.
		 * @param buffer Buffer to instantiate.
		 * @param bufferMemory Memory to use.
		 */
		static void createBuffer(PhysicalDevice* physicalDevice, LogicalDevice* logicalDevice, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory);

		/**
		 * Helper function to copy from one buffer to another.
		 *
		 * @param logicalDevice Logical device.
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 * @param srcBuffer Source buffer.  Must have been created with VK_BUFFER_USAGE_TRANSFER_SRC_BIT.
		 * @param dstBuffer Destination buffer.  Must have been created with VK_BUFFER_USAGE_TRANSFER_DST_BIT.
		 * @param size Number of bytes to copy.
		 */
		static void copyBuffer(LogicalDevice* logicalDevice, VkCommandPool pool, VkQueue queue, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);


		/**
		 * Helper function to issue a one-off set of commands.
		 *
		 * @param logicalDevice Logical device.
		 * @param pool Command pool to use to copy from staging buffer.
		 *
		 * @return Command buffer to execute.
		 */
		static VkCommandBuffer beginSingleTimeCommands(LogicalDevice* logicalDevice, VkCommandPool pool);

		/**
		 * Helper function to issue a one-off set of commands.
		 *
		 * @param commandBuffer Buffer to execute.
		 * @param logicalDevice Logical device.
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 */
		static void endSingleTimeCommands(VkCommandBuffer commandBuffer, LogicalDevice* logicalDevice, VkCommandPool pool, VkQueue queue);

		/*
		 * Helper function to create an image view.
		 *
		 * @param logicalDevice Logical device.
		 * @param image Image to create view for.
		 * @param format View format.
		 * @param aspectFlags Aspect flags.
		 */
		static VkImageView createImageView(LogicalDevice* logicalDevice, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);
	};

} // forge
