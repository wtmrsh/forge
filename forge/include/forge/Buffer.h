#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/PhysicalDevice.h"
#include "forge/LogicalDevice.h"

namespace forge
{

	/**
	 * Represents a generic buffer.
	 */
	class Buffer
	{
	protected:

		// Logical device
		LogicalDevice* mLogicalDevice;

		// Buffer handle
		VkBuffer mBuffer;

		// Underlying memory
		VkDeviceMemory mBufferMemory;

	public:

		/**
		 * Constructor.
		 */
		explicit Buffer(LogicalDevice* logicalDevice);

		/**
		 * Destructor.
		 */
		virtual ~Buffer() = default;

		/*
		 * Get buffer handle.
		 *
		 * @return Buffer handle
		 */
		VkBuffer getBuffer() const;

		/*
		 * Get buffer memory handle.
		 *
		 * @return Buffer memory handle
		 */
		VkDeviceMemory getBufferMemory() const;

	};

} // forge
