#pragma once
#include <string>
#include <functional>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"

namespace forge
{

	/**
	 * Helper function to check the result of a vulkan call.
	 * Throws BadResultCode if not VK_SUCCESS.

	 * @param result VkResult code to check.
	 * @param errMsg Message to pass to BadResultCode if result is not VK_SUCCESS.
	 *
	 */
	void checkResult(VkResult result, std::string const& errMsg);

	/**
	 * Enum to specify message severity.
	 */
	enum class MessageLevel
	{
		Debug,
		Info,
		Warning,
		Error,
		Fatal
	};

	/**
	 * Message callback handler.
	 */
	typedef std::function<void(std::string const&, MessageLevel)> MessageHandler;

	/**
	 * Struct for outputting messages.
	 */
	struct MessageHandlerInfo
	{
		MessageHandler handler;
		MessageLevel level;
	};

	/**
	 * Output a message to any attached handlers.
	 *
	 * @param level Severity level of the specified message.
	 * @param msg Message text.
	 */
	void message(MessageLevel level, std::string const& msg);

} // forge