#pragma once

#include <vector>

#include "forge/Platform.h"

namespace forge
{

	/**
	 * Struct for storing PhysicalDevice settings.
	 */
	struct PhysicalDeviceParameters
	{
		// Vulkan extensions that are required.
		std::vector<char const*> requiredExtensions;
	};

} // forge