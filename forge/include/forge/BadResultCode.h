#pragma once

#include <stdexcept>
#include <string>

#include <vulkan/vulkan.h>

#include "Platform.h"
#include "VulkanException.h"

/**
 * Exception handler for vulkan error codes.
 */
class BadResultCode : public VulkanException
{
	VkResult mResult;

	std::string mExplanation;

public:

	/**
	 * Constructor.
	 *
	 * @param result Vulkan result code.
	 */
	BadResultCode(VkResult result, std::string const& errMsg)
		: VulkanException(errMsg)
		, mResult(result)
	{
		switch (result)
		{
		case VK_SUCCESS:
			mExplanation = "Call was successful (not an exception)."; break;

		case VK_ERROR_OUT_OF_HOST_MEMORY:
			mExplanation = "Out of host memory."; break;

		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			mExplanation = "Out of device memory."; break;

		case VK_ERROR_INITIALIZATION_FAILED:
			mExplanation = "Initialization failed."; break;

		case VK_ERROR_LAYER_NOT_PRESENT:
			mExplanation = "Layer not present."; break;

		case VK_ERROR_EXTENSION_NOT_PRESENT:
			mExplanation = "Extension not present."; break;

		case VK_ERROR_INCOMPATIBLE_DRIVER:
			mExplanation = "Incompatible driver."; break;

		default:
			mExplanation = "Unhandled error from vulkan.";
		}
	}

	/**
	 * Return the result code.
	 *
	 * @return the stored result code.
	 */
	VkResult getResult() const
	{
		return mResult;
	}

	/**
	 * Return the textual explanation of the result code.
	 *
	 * @return the stored explanation.
	 */	
	std::string getExplanation() const
	{
		return mExplanation;
	}
};
