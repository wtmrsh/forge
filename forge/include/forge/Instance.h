#pragma once
#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/InstanceParameters.h"
#include "forge/SurfaceFactory.h"

namespace forge
{

	/**
	 * Encapsulate a VkInstance.  This is the connection between the application and the library.
	 */
	class Instance
	{
		InstanceParameters mParameters;

		VkInstance mInstance;

		VkDebugUtilsMessengerEXT mDebugMessenger;

	private:
	
		/**
		 * Get the supported extensions, and check if the required ones are supported.
		 *
		 * @param requiredExtensions Extensions needed by the instance.
		 * @param numRequiredExtensions The number of extensions passed in first parameter.
		 */
		void checkExtensionSupport(std::vector<char const*> const& requiredExtensions);

		/**
		 * Check validation layers.
		 *
		 * @param requiredLayers Layers to check for, and use.
		 */
		void checkValidationLayers(std::vector<char const*> const& requiredValidationLayers);

		/**
		 * Create vulkan instance.
		 */
		void create();

		/**
		 * Validation layer callback.
		 *
		 * @param messageSeverity Severity of the message.
		 * @param messageType Type/origin of message.
		 * @param pCallbackData Originating data struct.
		 * @param pUserData User data.
		 *
		 * @return !!TODO
		 */
		static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
			VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
			VkDebugUtilsMessageTypeFlagsEXT messageType,
			VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData,
			void* pUserData);

	public:

		/**
		 * Constructor.
		 *
		 * @param parameters Parameters to create instance with.
		 */
		Instance(InstanceParameters const& parameters);

		/**
		 * Destructor.
		 */
		virtual ~Instance();

		/**
		 * Get the physical devices supported on this instance.
		 *
		 * @return List of devices to choose from.
		 */
		std::vector<VkPhysicalDevice> getPhysicalDevices() const;

		/**
		 * Create a surface to render onto.
		 *
		 * @param surfaceFactory SurfaceFactory instance to create surface with.
		 *
		 * @return Surface handle.
		 */
		VkSurfaceKHR createSurface(SurfaceFactory surfaceFactory);

		/**
		 * Destroy a surface handle.
		 *
		 * @param surface Surface handle.
		 */
		void destroySurface(VkSurfaceKHR* surface);

		/**
		 * Create debug messenger for outputting validation layer messages.
		 */
		void createDebugMessenger();

		/**
		 * Destroy debug messenger.
		 */
		void destroyDebugMessenger();
	};

} // forge