#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/PhysicalDevice.h"
#include "forge/LogicalDevice.h"

namespace forge
{

	/**
	 * Represents an image.
	 */
	class Image
	{
	protected:

		// Logical device
		LogicalDevice* mLogicalDevice;

		// Data
		std::vector<char> mData;

		// Dimensions
		int mWidth, mHeight;

		// Number of channels, typically 3 or 4.
		int mNumChannels;

		// Image handle
		VkImage mImage;

		// Underlying memory
		VkDeviceMemory mImageMemory;

		// View
		VkImageView mImageView;

	protected:

		/**
		 * Check if a format supports stencilling.
		 *
		 * @param format Format.
		 *
		 * @return Whether the format supports stencilling or not.
		 */
		bool hasStencilComponent(VkFormat format) const;

		/**
		 * Create image object.
		 *
		 * @param physicalDevice Physical device to use.
		 * @param format Image format.
		 * @param tiling Image tiling.
		 * @param usage Image usage.
		 * @param properties Image properties.
		 *
		 */
		void createImage(PhysicalDevice* physicalDevice, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties);

		/**
		 * Change the layout layout.
		 *
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 * @param format Image format.
		 * @param oldLayout Old layout.
		 * @param newLayout New layout.
		 *
		 */
		void transitionImageLayout(VkCommandPool pool, VkQueue queue, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);

		/**
		 * Copy specified buffer to the image.
		 *
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 * @param buffer Source buffer.
		 */
		void copyBufferToImage(VkCommandPool pool, VkQueue queue, VkBuffer buffer);

		/**
		 * Create image view for the image object.
		 *
		 * @param format Format to use.
		 * @param aspectFlags Aspect flags.
		 */
		void createImageView(VkFormat format, VkImageAspectFlags aspectFlags);

	public:

		/**
		 * Constructor.
		 *
		 * @param logicalDevice Logical device to use.
		 * @param data Decoded image data, interleaved.
		 * @param width Image width.
		 * @param height Image height.
		 * @param numChannels Number of channels.
		 *
		 */
		Image(LogicalDevice* logicalDevice, std::vector<char> const& data, int width, int height, int numChannels);

		/**
		 * Destructor.
		 */
		virtual ~Image();

		/**
		 * Get width.
		 *
		 * @return Image width.
		 */
		int getWidth() const;

		/**
		 * Get height.
		 *
		 * @return Image height.
		 */
		int getHeight() const;

		/**
		 * Get number of channels.
		 *
		 * @return Number of channels.
		 */
		int getNumChannels() const;

		/**
		 * Get image view handle.
		 *
		 * @return Image view handle.
		 */
		VkImageView getImageView() const;

		/**
		 * Create and load the image data.
		 *
		 * @param physicalDevice Physical device to use.
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 * @param clearData Whether or not to clear out the source data once created.
		 *        Set to true if you don't need to recreate the texture
		 */
		virtual void create(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue, bool clearData);

		/**
		 * Destroy image.
		 */
		void destroy();

	};

} // forge
