#pragma once
#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/QueueFamily.h"
#include "forge/SwapchainSupport.h"
#include "forge/PhysicalDeviceParameters.h"
#include "forge/PhysicalDeviceCaps.h"

namespace forge
{

	/**
	 * Encapsulate a VkPhysicalDevice.
	 */
	class PhysicalDevice
	{
		// Creation parameters
		PhysicalDeviceParameters mParameters;

		// Device handle
		VkPhysicalDevice mDevice;

	private:

		/* Test whether a physical device can be used.
		 *
		 * @param device The device to check.
		 * @param surface Surface in use.
		 * @param extensions Required extensions.
		 */
		bool isSuitable(VkPhysicalDevice device, VkSurfaceKHR surface, std::vector<char const*> const& extensions) const;

		/**
		 * Give a physical device a score based on its capabilities.
		 *
		 * @param device Device handle.
		 * @param properties Device properties.
		 *
		 * @return Score, where higher the better.
		 */
		int getScore(VkPhysicalDevice device, VkPhysicalDeviceProperties properties) const;

		/**
		 * Check a set of extensions is supported.
		 *
		 * @param device The device to check.
		 * @param requiredExtensions The extensions to check for.
		 */
		void checkExtensionSupport(VkPhysicalDevice device, std::vector<char const*> const& requiredExtensions) const;

		/*
		 * Get the indices for the required queue families.
		 *
		 * @param device The device to check.
		 * @param surface Surface in use.
		 *
		 * @return indices.
		 */
		QueueFamilyIndices _getQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface) const;

		/**
		 * Get details of swapchain for a given physical device.
		 *
		 * @param device The device to check.
		 * @param surface Surface in use.
		 *
		 * @return Details.
		*/
		SwapchainSupport _getSwapchainSupport(VkPhysicalDevice device, VkSurfaceKHR surface) const;

	public:

		/**
		 * Constructor.
		 *
		 * @param choices List of possible devices to choose from.
		 * @param surface Surface in use.
		 * @param parameters Parameters to create physical device with.
		 */
		PhysicalDevice(std::vector<VkPhysicalDevice> const& choices, VkSurfaceKHR surface, PhysicalDeviceParameters const& parameters);

		/*
		 * Destructor.
		 */
		virtual ~PhysicalDevice();

		/*
		 * Get the indices for the required queue families.
		 *
		 *
		 * @param surface Surface in use.
		 *
		 * @return Indices.
		 */
		QueueFamilyIndices getQueueFamilies(VkSurfaceKHR surface) const;

		/**
		 * Get details of swapchain for a given physical device.
		 *
		 * @param surface Surface in use.
		 *
		 * @return Details.
		*/
		SwapchainSupport getSwapchainSupport(VkSurfaceKHR surface) const;

		/**
		 * Get device capabilities.
		 *
		 * @return Device caps.
		 */
		PhysicalDeviceCaps getCaps() const;

		/**
		 * Create a logical device.
		 *
		 * @param createInfo Creation parameters.
		 *
		 * @return VkDevice instance.
		 */
		VkDevice createLogicalDevice(VkDeviceCreateInfo const& createInfo) const;

		/*
		 * Find the appropriate type of memory to use for allocation of a particular object.
		 *
		 * @param typeFilter The type required.
		 * @param properties Properties required.
		 *
		 * @return Appropriate memory type.
		 */
		uint32_t getMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) const;

		/*
		 * Get a format from a list of candidates which support the required tiling and features.
		 *
		 * @param candidates List of format candidates to choose from.
		 * @param tiling Required tiling.
		 * @param features Required features.
		 *
		 * @return The format which fulfils the requirements.
		 */
		VkFormat getSupportedFormat(std::vector<VkFormat> const& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) const;
	};

} // forge