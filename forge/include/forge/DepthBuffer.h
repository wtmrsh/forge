#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "forge/Platform.h"
#include "forge/Image.h"

namespace forge
{

	/**
	 * Represents a depth buffer.
	 */
	class DepthBuffer : public Image
	{
	public:

		/**
		 * Constructor.
		 *
		 * @param logicalDevice Logical device to use.
		 * @param width Image width.
		 * @param height Image height.
		 *
		 */
		DepthBuffer(LogicalDevice* logicalDevice, int width, int height);

		/**
		 * Create the depth buffer.
		 *
		 * @param physicalDevice Physical device to use.
		 * @param pool Command pool to use to copy from staging buffer.
		 * @param queue Queue to perform the staging copy on.
		 */
		void create(PhysicalDevice* physicalDevice, VkCommandPool pool, VkQueue queue);

		/*
		 * Get the requested depth format.
		 *
		 * @param physicalDevice Physical device to use.
		 *
		 * @return Format.
		 */
		VkFormat getDepthFormat(PhysicalDevice* physicalDevice) const;
	};

} // forge
