#pragma once

#include <cstdint>

#define FORGE_NAME "Forge"

// Platform settings - based off OGRE3D (www.ogre3d.org)
#define FORGE_PLATFORM_WIN32 1
#define FORGE_PLATFORM_LINUX 2
#define FORGE_PLATFORM_APPLE 3

#define FORGE_COMPILER_MSVC 1
#define FORGE_COMPILER_GNUC 2
#define FORGE_COMPILER_BORL 3

// Find compiler information
#if defined( _MSC_VER )
#   define FORGE_COMPILER FORGE_COMPILER_MSVC
#   define FORGE_COMP_VER _MSC_VER
#elif defined( __GNUC__ )
#   define FORGE_COMPILER FORGE_COMPILER_GNUC
#   define FORGE_COMP_VER (((__GNUC__)*100) + \
        (__GNUC_MINOR__*10) + \
        __GNUC_PATCHLEVEL__)
#elif defined( __BORLANDC__ )
#   define FORGE_COMPILER FORGE_COMPILER_BORL
#   define FORGE_COMP_VER __BCPLUSPLUS__
#else
#   pragma error "Unknown compiler."

#endif

// Set platform
#if defined( __WIN32__ ) || defined( _WIN32 )
#   define FORGE_PLATFORM FORGE_PLATFORM_WIN32
#elif defined( __APPLE_CC__)
#   define FORGE_PLATFORM FORGE_PLATFORM_APPLE
#else
#   define FORGE_PLATFORM FORGE_PLATFORM_LINUX
#endif

// DLL Export
#if FORGE_PLATFORM == FORGE_PLATFORM_WIN32
#	if defined(FORGE_DLL_EXPORT)
#		define _FORGEAPI __declspec( dllexport )
#	elif defined(FORGE_STATIC_LIB)
#		define _FORGEAPI
#	else
#		if defined(__MINGW32__)
#			define _FORGEAPI
#		else
#			define _FORGEAPI __declspec( dllimport )
#		endif
#	endif
#elif FORGE_PLATFORM == FORGE_PLATFORM_LINUX
#	if defined(FORGE_DLL_EXPORT)
#		define _FORGEAPI __attribute__((visibility("default")))
#	else
#		define _FORGEAPI
#	endif
#endif

// Disable warning on non-exported templates.
#pragma warning( disable: 4251 )
