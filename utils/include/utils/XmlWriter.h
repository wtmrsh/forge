#pragma once

#include <string>
#include <vector>

#include "StringUtils.h"
#include "Platform.h"

namespace utils
{

	class XmlWriteNode
	{
		friend class XmlWriter;

	private:

		void* mDocument;

		void* mNode;

		std::vector<XmlWriteNode*> mChildren;

	private:

		XmlWriteNode(std::string const& name, void* parent, void* document);

	public:

		~XmlWriteNode();

		XmlWriteNode* createChild(std::string const& name);

		void setValue(std::string const& value);

		void addAttribute(std::string const& name, std::string const& value);
	};

	class XmlWriter
	{
		void* mDocument;

		XmlWriteNode* mRootNode;

	public:

		explicit XmlWriter(std::string const& rootName);

		~XmlWriter();

		XmlWriteNode* getRootNode();

		void write(std::string const& filepath);
	};

} // utils