#include "XmlWriter.h"
#include "StringUtils.h"
#include "tinyxml2.h"

namespace utils
{
	using namespace std;

	XmlWriteNode::XmlWriteNode(string const& name, void* parent, void* document)
		: mNode(nullptr)
		, mDocument(document)
	{
		mNode = static_cast<tinyxml2::XMLDocument*>(mDocument)->NewElement(name.c_str());

		if (parent)
		{
			static_cast<tinyxml2::XMLElement*>(parent)->InsertEndChild(static_cast<tinyxml2::XMLElement*>(mNode));
		}
		else
		{
			static_cast<tinyxml2::XMLDocument*>(mDocument)->InsertEndChild(static_cast<tinyxml2::XMLElement*>(mNode));
		}
	}

	XmlWriteNode::~XmlWriteNode()
	{
		for (auto node: mChildren)
		{
			delete node;
		}
	}

	XmlWriteNode* XmlWriteNode::createChild(string const& name)
	{
		auto node = new XmlWriteNode(name, this, mDocument);
		mChildren.push_back(node);
		return node;
	}

	void XmlWriteNode::setValue(string const& value)
	{
		static_cast<tinyxml2::XMLElement*>(mNode)->SetText(value.c_str());
	}

	void XmlWriteNode::addAttribute(string const& name, string const& value)
	{
		static_cast<tinyxml2::XMLElement*>(mNode)->SetAttribute(name.c_str(), value.c_str());
	}

	XmlWriter::XmlWriter(string const& rootName)
	{
		mDocument = new tinyxml2::XMLDocument();
		auto doc = static_cast<tinyxml2::XMLDocument*>(mDocument);

		auto decl = doc->NewDeclaration();
		doc->LinkEndChild(decl);

		mRootNode = new XmlWriteNode(rootName, nullptr, mDocument);
	}

	XmlWriter::~XmlWriter()
	{
		delete static_cast<tinyxml2::XMLDocument*>(mDocument);
	}

	XmlWriteNode* XmlWriter::getRootNode()
	{
		return mRootNode;
	}

	void XmlWriter::write(std::string const& filepath)
	{
		static_cast<tinyxml2::XMLDocument*>(mDocument)->SaveFile(filepath.c_str());
	}

} // utils